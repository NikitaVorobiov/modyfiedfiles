var express = require('express');
var db = require("./db/db.js");
db.connect();
var app = express();
var passport = require('passport');
require('./lib/config/passport')(passport); // pass passport for configuration
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var session = require('express-session');
var resumable = require('./lib/resumable/resumable-node.js')('./lib/resumable/tmp');
var multipart = require('connect-multiparty');

app.set('port', (process.env.PORT || 5000));

// required for passport

app.use(session({
	secret: 'bobo'
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

// get all data/stuff of the body (POST) parameters
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({
	type: 'application/vnd.api+json'
})); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({
	extended: true
})); // parse application/x-www-form-urlencoded

app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
//app.use(express.static(__dirname + '/public'));
var path = require('path');
app.use(express.static(path.join(__dirname, 'app')));
app.set('views', __dirname + '/app/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(multipart());

var storage = {};
if (process.env.env == "production") {
	storage.AWS_ACCESS_KEY = process.env.AWS_ACCESS_KEY;
	storage.AWS_SECRET_KEY = process.env.AWS_SECRET_KEY;
	storage.S3_BUCKET = process.env.S3_BUCKET;
	storage.S3_BUCKET_REGION = process.env.S3_BUCKET_REGION;
} else {
	var fs = require("fs");
	storage = JSON.parse(fs.readFileSync('./AwsConfig.json', 'utf8'));
}
storage.aws = require('aws-sdk');

require('./lib/config/routes')(app, passport, resumable, storage);

app.listen(app.get('port'), function() {
	console.log("Node app is running at localhost:" + app.get('port'));
});

exports = module.exports = app; // expose app
