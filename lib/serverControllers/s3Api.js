module.exports = function(storage){
    var storageHost = 'https://'+storage.S3_BUCKET+'.s3.amazonaws.com/';
    storage.aws.config.update({accessKeyId: storage.AWS_ACCESS_KEY, secretAccessKey: storage.AWS_SECRET_KEY, region:storage.S3_BUCKET_REGION});
    var s3 = new storage.aws.S3();

    return {
        s3ListObjects: function(req, res, next){
            var prefix = req.query.prefix;
            var s3_params = {
                Bucket: storage.S3_BUCKET,
                Prefix: prefix,
                MaxKeys: 3000
            };
            if(req.query.delimiter){
                s3_params.Delimiter = req.query.delimiter;
            }
            s3.listObjects(s3_params, function(err, data) {
                if (err){
                    console.log("error in getting list of objects: " + err);
                    throw (err);
                } else if(req.commonPrefixes) {
                    req.keys = data.CommonPrefixes.map(function(content){
                        return {Key: content.Prefix};
                    });
                    next();
                } else {
                    req.keys = data.Contents.map(function(content){
                        return {Key: content.Key};
                    });
                    next();
                }
            });
        },
        sign_s3: function(req, res, next){
            var s3_params = {
                Bucket: storage.S3_BUCKET,
                Key: req.query.s3_object_name,
                ContentType: req.query.s3_object_type,
                ACL: 'public-read'
            };
            s3.getSignedUrl('putObject', s3_params, function(err, data){
                if(err){
                    console.log("error in getting signed url: " + err);
                    throw (err);
                }
                else{
                    req.return_data = {
                        signed_request: data,
                        url: storageHost + req.query.s3_object_name
                    };
                }
                next();
            });
        },
        upload: function(req, res, next){
            var s3_params = {
                Bucket: storage.S3_BUCKET,
                Key: req.body.key + req.body.s3_object_name,
                Body: new Buffer(req.binaryData, 'binary'),
                ContentType: req.body.s3_object_type,
                ACL: 'public-read'
            };
            s3.upload(s3_params, function(err, data){
                if(err){
                    console.log("error in uploading data: " + err);
                    throw (err);
                }
                else{
                    console.log("uploaded data to " + data.Location);
                }
                next();
            });
        },
        deleteObjects: function(req, res, next){
            if(req.keys.length){
                var params = {
                    Bucket: storage.S3_BUCKET,
                    Delete: {
                        Objects: req.keys
                    }
                };
                s3.deleteObjects(params, function(err, data) {
                    if (err){
                        console.log("error in deleting objects: ", err);
                        throw (err);
                    } else{
                        console.log("deleted " + data.Deleted.length + " files");
                        req.deleted = data.Deleted.length;
                        if(next && typeof next === "function"){
                            next();
                        }
                    }
                });
            } else {
                console.log("no files to delete");
                req.deleted = 0;
                if(next && typeof next === "function"){
                    next();
                }
            }

        }
    };
};