  passport = require('passport');

/**
 * Session
 * returns info on authenticated user
 */
exports.session = function (req, res) {
    var user = {_id:req.user._id, email:req.user.email, type: req.user.type, firstName: req.user.firstName, lastName: req.user.lastName};
    res.json(user);
};

/**
 * Logout
 * returns nothing
 */
exports.logout = function (req, res) {
  if(req.user) {
    req.logout();
    res.sendStatus(200);
  } else {
    res.sendStatus(400, "Not logged in");
  }
};

/**
 *  Login
 *  requires: {email, password}
 */
exports.login = function (req, res, next) {
  passport.authenticate('local-login', function(err, user, info) {
    var error = err || info;
    if (error) { return res.json(400, error); }
    req.logIn(user, function(err) {
      if (err) { return res.send(err); }
      var user = {_id:req.user._id, email:req.user.email, type: req.user.type, firstName: req.user.firstName, lastName: req.user.lastName};
      res.json(user);
    });
  })(req, res, next);
};