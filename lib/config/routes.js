var auth = require('./auth');
var http = require('http');
var im = require('../serverControllers/imagemagick');
var objectMerge = require('object-merge');
var wkhtmltopdf = require('../serverControllers/wkhtmltopdf');
var accum = require('accum');
var rp = require('request-promise');
var crypto = require('crypto-js');
var nodemailer = require('nodemailer');
var XMLWriter = require('xml-writer');
var transporter = nodemailer.createTransport({
	service: 'Gmail',
	auth: {
		user: 'nodemailertestss@gmail.com',
		pass: 'testqwerty'
	}
});

var mailOptions = {
	from: 'Slavik <nodemailertestss@gmail.com>',
	to: 'nichita.ursu.v@gmail.com',
	subject: 'Hello',
	html: '<b>test</b>'
};

module.exports = function(app, passport, resumable, storage) {
	var db = require("./../../db/db.js");
	var session = require('../serverControllers/session');
	var s3Api = require('../serverControllers/s3Api')(storage);
	var routesUtils = require('../serverControllers/routesUtils');

	//	Correct implementation
	var path = require('path');

	/*********************************/
	// todo: remove all test api calls

	app.get('/getAllUsers', function(request, response) {
		db.user.getAllUsers(function(res) {
			response.send(res);
		});
	});

	app.post('/galleries/:gallery', function(req, res) {
		console.log(req.data);
	});

	app.get('/getAllGalleries', function(request, response) {
		db.gallery.getAllGalleries(function(res) {
			response.send(res);
		});
	});

	app.get('/join/:gal', function(request, response) {
		db.gallery.getAllGalleries(function(res) {
			response.send({data: res, param: request.params.gal});
		});
	});

	app.get('/removeAllUsers', function(request, response) {
		db.user.removeAllUsers(function(res) {
			response.send(res);
		});
	});

	app.get('/removeAllGalleries', function(request, response) {
		db.gallery.removeAllGalleries(function(res) {
			response.send(res);
		});
	});

	app.get('/getAllThemes', function(request, response) {
		db.theme.getAllThemes(function(res) {
			response.send(res);
		});
	});

	// var secretKey = '076f75fab623485b1accab2d41089577';
	// var rpData = new XMLWriter;
	// rpData.startDocument();
	// rpData.startElement('image_process_call').startElement('image_url').writeAttribute('order', '1').text('http://s.funny.pho.to/c8b21ae-008/images/samples/boy.jpg')
	// .endElement().startElement('method_list').startElement('method').writeAttribute('order', '1').writeElement('name', 'collage').writeElement('params', 'template_name=phone_in_hands')
	// .endElement().endElement().startElement('result_size').text('1400').endElement().startElement('result_quality').text('90').endElement().startElement('template_watermark')
	// .text('true').endElement().startElement('thumb1_size').text('200').endElement().startElement('thumb1_quality').text('85').endElement().startElement('lang').text('en').endElement().endElement();
	// rpData.endDocument();
	// console.log(prData.toString());
	// var signData = crypto.HmacSHA1(rpData.output, secretKey).toString();
	// var appId = 'aca60680922cec635d12d8974d3a55da';
	// console.log(rpData.output);
	// console.log(signData);

	// var bodyRequest = 'app_id=' + appId + '&data=' + encodeURIComponent(rpData.output) + '&sign_data=' + signData;
	// console.log(bodyRequest);

	// var rpOptions = {
	// 	method: 'POST',
	// 	uri: 'http://opeapi.ws.pho.to/addtask',
	// 	form: bodyRequest
	// };
	//
	// rp(rpOptions)
	// 	.then(function success (parsedBody) {
	// 		console.log(parsedBody);
	// 	}, function error (err) {
	// 		console.log(err);
	// 	});

		var appId = 'aca60680922cec635d12d8974d3a55da';
		var rpData = "<image_process_call><image_url>http://s3.amazonaws.com/temp.ws.pho.to/2652dca9b24436a68dece3e61eddcd1b0ec1e700.jpeg</image_url><methods_list><method><name>collage</name><params>template_name=45909;user_template=1</params></method></methods_list><lang>en</lang><result_size>1080</result_size><thumb1_size>200</thumb1_size><template_watermark>false</template_watermark></image_process_call>";
		var secretKey = '076f75fab623485b1accab2d41089577';
		var signData = crypto.HmacSHA1(rpData, secretKey).toString();
		var bodyRequest = 'app_id=' + appId + '&data=' + encodeURIComponent(rpData) + '&sign_data=' + signData;


		rpOptions = {
			method: 'POST',
			uri: 'http://opeapi.ws.pho.to/addtask',
			form: bodyRequest
		};

		rp(rpOptions)
			.then( function success (response) {
				console.log(response);
			}, function error (err) {
				console.log(err);
			})

		rp('http://opeapi.ws.pho.to/getresult?request_id=68e31206-910e-4734-b06c-611dd7ca9bd0')
			.then( function success (res) {
				console.log(res);
			}, function error (err) {
				console.log(err);
			})

	//***************************************************
	//					 Test APIs

	app.get('/getGroups/:groupId', function(request, response) {
		db.gallery.getGalleries(request.params.groupId, function(res) {
			response.send(res);
		});
	});

	app.post('/updateUserGroups', function(request, response) {
		db.user.updateUserGroups(request.body, function(res) {
			response.send(res);
		});
	});

	app.post('/updateGroupMembers', function(request, response) {
		db.gallery.updateGroupMembers(request.body, function(res) {
			response.send(res);
		})
	});

	app.post('/updateGroupChildren', function(request, response) {
		db.gallery.updateGroupChildren(request.body, function(res) {
			response.send(res);
		});
	});

	app.post('/inviteViaEmail', function(request, response) {
		var mailOptions,
			nodemailer,
			transporter;
		nodemailer = require('nodemailer');
		transporter = nodemailer.createTransport({
			service: 'Gmail',
			auth: {
				user: 'nodemailertestss@gmail.com',
				pass: 'testqwerty'
			}
		});

		mailOptions = {
			from: 'CreateAlbums Team <nodemailertestss@gmail.com>',
			to: request.body.recipient,
			html: 'Hello, you was invited in ' + request.body.groupName + '. Follow this link , to join this group :' + "<a href='" + request.body.joinLink + "'> Join </a>",
			subject: 'Invite'
		};

		transporter.sendMail(mailOptions, function(err, info) {
			if (err) {
				return console.log(err);
			}
			return console.log("Message sent: " + info.response);
		});

		response.send('Message was send to : ' + request.body.recipient);
	});

	app.post('/sendEmail', function(request, response) {
		var mailOptions,
			nodemailer,
			transporter;
		nodemailer = require('nodemailer');
		transporter = nodemailer.createTransport({
			service: 'Gmail',
			auth: {
				user: 'nodemailertestss@gmail.com',
				pass: 'testqwerty'
			}
		});

		mailOptions = {
			from: 'CreateAlbums Team <nodemailertestss@gmail.com>',
			to: request.body.recipient,
			html: request.body.html,
			subject: request.body.subject
		};

		transporter.sendMail(mailOptions, function(err, info) {
			if (err) {
				return console.log(err);
			}
			return console.log("Message sent: " + info.response);
		});

		response.send('Message was send to : ' + request.body.recipient);
	});

	//***************************************************
	app.get('/removeAllThemes', function(request, response) {
		db.theme.removeAllThemes(function(res) {
			response.send(res);
		});
	});

	app.get('/getAllTemplates', function(request, response) {
		db.template.getAllTemplates(function(res) {
			response.send(res);
		});
	});

	app.get('/removeAllTemplates', function(request, response) {
		db.template.removeAllTemplates(function(res) {
			response.send(res);
		});
	});

	app.get('/getAllAlbums', function(request, response) {
		db.album.getAllAlbums(function(res) {
			response.send(res);
		});
	});

	app.get('/removeAllAlbums', function(request, response) {
		db.album.removeAllAlbums(function(res) {
			response.send(res);
		});
	});

	/*********************************/
	// end of todo: remove all test api calls

	app.get('/getUser/:id', function(request, response) {
		db.user.getUser(request.params.id, function(res) {
			response.send(res);
		});
	});

	app.get('/removeGallery', function(request, response) {
		db.gallery.removeGallery(request.query.id, function(res) {
			response.send(res);
		});
	});

	app.post('/updateUser', function(request, response) {
		db.user.updateUser({
			_id: request.body.id,
			name: request.body.name,
			email: request.body.email
		}, function(res) {
			response.send(res);
		});
	});

	app.post('/updateUserGroups', function(request, response) {
		db.user.getUser(request.body.userId, function(res) {
			// response.send(res);
			console.log(res + '<<<=== this is what you need !!!');
			res.groups.push(request.body.groupInfo);
			res.save({
				groups: res.groups
			}, function() {
				response.send(res)
			});
		});
	});

	app.get('/removeUser', function(request, response) {
		db.user.removeUser(request.query.id, function(res) {
			response.send(res);
		});
	});

	// process the signup form
	app.post('/createUser', function(req, res) {
		passport.authenticate('local-signup', function(err, user, info) {
			if (user) {
				req.login(user, null, function() {
					user = {
						_id: user._id,
						email: user.email,
						type: user.type,
						firstName: user.firstName,
						lastName: user.lastName,
						sysType: user.sysType
					};
					res.jsonp({err: err, user: user, info: info}); //TODO: encodeToken
				});
			} else {
				console.log("error createUser: ", err);
				res.jsonp({err: err, user: user, info: info}); //TODO: encodeToken
			}
		})(req, res);
	});

	app.post('/createGallery', function(req, res) {
		db.gallery.createGallery(req.body, function(newGallery) {
			if (!newGallery.error) {
				res.jsonp(newGallery);
			} else {
				console.log("error createGallery: ", newGallery.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.post('/updateGallery', function(req, res) {
		db.gallery.updateGallery(req.body, function(gallery) {
			if (!gallery.error) {
				res.jsonp(gallery);
			} else {
				console.log("error updateGallery: ", gallery.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.get('/getGalleries', function(req, res) {
		db.gallery.getGalleries(req.query, function(galleries) {
			res.send(galleries);
		});
	});

	app.get('/getGalleryContent', function(req, res) {
		db.gallery.getGalleryContent(req.query, function(gallery) {
			res.send(gallery);
		});
	});

	app.get('/getAllUserThemes', function(req, res) {
		db.theme.getAllUserThemes(req.query, function(galleries) {
			res.send(galleries);
		});
	});

	app.get('/getAllThemesNames', function(req, res) {
		db.theme.getAllThemesNames(req.query, function(galleries) {
			res.send(galleries);
		});
	});

	app.get('/getChildInGallery', function(req, res) {
		db.gallery.getChildInGallery(req.query, function(child) {
			res.send(child);
		});
	});

	app.post('/getOrCreateThemes', function(req, res) {
		db.theme.getOrCreateThemes(req.body, function(themes) {
			if (!themes.error) {
				res.jsonp(themes);
			} else {
				console.log("error getOrCreateThemes: ", themes.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.post('/addThemeDesign', function(req, res) {
		db.theme.addThemeDesign(req.body, function(theme) {
			if (!theme.error) {
				res.jsonp(theme);
			} else {
				console.log("error addThemeDesign: ", theme.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.post('/updateThemeDesign', function(req, res) {
		db.theme.updateThemeDesign(req.body, function(theme) {
			if (!theme.error) {
				res.jsonp(theme);
			} else {
				console.log("error updateThemeDesign: ", theme.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.get('/sign_s3', s3Api.sign_s3, function(req, res) {
		if (req.err) {
			console.log("error sign_s3: ", req.err);
			res.status(400);
			res.send();
		} else {
			res.jsonp(req.return_data);
		}
	});

	app.get("/getThumbsList", s3Api.s3ListObjects, function(req, res) {
		if (req.error) {
			console.log("error getThumbsList: ", req.error);
			res.status(400);
			res.send();
		} else {
			res.jsonp({keys: req.keys});
		}
	});

	app.get("/getThumbsListInGallery", s3Api.s3ListObjects, function(req, res) {
		if (req.error) {
			console.log("error getThumbsListInGallery: ", req.error);
			res.status(400);
			res.send();
		} else {
			//filter non-thumbs and return as objects with themeId as key
			var thumbs = {};
			req.keys.forEach(function(keyObj) {
				var split = keyObj.Key.split("/");
				if (split.length == 8) {
					var themeId = split[5];
					if (thumbs[themeId]) {
						thumbs[themeId].push({src: keyObj.Key});
					} else {
						thumbs[themeId] = [
							{
								src: keyObj.Key
							}
						];
					}
				}
			});
			res.jsonp(thumbs);
		}
	});

	app.post('/resizeImage', function(req, res, next) {
		var path = req.body.path;
		var splitPath = path.split("/");
		path = path.replace(/ /g, '%20').replace("https", "http");
		req.body.s3_object_name = splitPath[splitPath.length - 1] + 'Thumb';
		console.log("resize path = ", path);
		im.resize({
			srcPath: path,
			width: 500,
			height: 300
		}, function(err, stdout) {
			if (err) {
				console.log("error in resizing: " + err);
				res.status(400);
				res.send();
			} else {
				req.binaryData = stdout;
				next();
			}
		});
	}, s3Api.upload, function(req, res) {
		res.jsonp({resized: true})
	});

	app.post('/splitImage', function(req, res, next) {
		console.log(req.body);
		var dim = req.body.dim;
		var gravity = req.body.gravity;
		var path = req.body.path;
		var splitPath = path.split("/");
		path = path.replace(/ /g, '%20');
		var suffix = gravity == "west"
			? "Left"
			: "Right";
		var fileName = splitPath[splitPath.length - 1];
		req.body.s3_object_name = fileName + suffix;
		req.keys = [
			{
				Key: req.body.key + fileName
			}
		];
		req.body.key += 'split/';
		console.log("split path = ", path);
		im.crop({
			srcPath: path,
			width: dim.width / 2,
			height: dim.height,
			gravity: gravity
		}, function(err, stdout) {
			if (err) {
				console.log("error in split: " + err);
				res.status(400);
			} else {
				req.binaryData = stdout;
				next();
				res.status(200);
			}
			res.send();
		});
	}, s3Api.upload, s3Api.deleteObjects);

	app.post('/rotateImage', function(req, res, next) {
		var path = req.body.path;
		var splitPath = path.split("/");
		path = path.replace(/ /g, '%20');
		req.body.s3_object_name = splitPath[splitPath.length - 1];
		console.log("rotate path = ", path);
		im.resize({
			rotateType: req.body.rotateType,
			srcPath: path
		}, function(err, stdout) {
			if (err) {
				console.log("error in resizing: " + err);
				res.status(400);
				res.send();
			} else {
				req.binaryData = stdout;
				next();
			}
		});
	}, s3Api.upload, function(req, res) {
		res.jsonp({rotated: true})
	});

	app.post('/createTemplate', function(req, res) {
		console.log(req.body);
		db.template.createTemplate(req.body, function(newTemplate) {
			if (!newTemplate.error) {
				res.jsonp(newTemplate);
			} else {
				console.log("error createTemplate: ", newTemplate.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.post('/updateTemplate', function(req, res) {
		db.template.updateTemplate(req.body, function(updatedTemplate) {
			if (!updatedTemplate.error) {
				res.jsonp(updatedTemplate);
			} else {
				console.log("error updateTemplate: ", updatedTemplate.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.get('/getTemplates', function(req, res) {
		db.template.getTemplates(req.query, function(templates) {
			res.send(templates);
		});
	});

	app.get('/getTemplate/:templateId', function(req, res) {
		db.template.getTemplates(req.params.templateId, function(templates) {
			res.send(templates);
		});
	});

	app.get('/getTemplateContent/:templateId', function(request, response) {
		db.template.getTemplateContent(request.params, function(content) {
			response.send(content)
		});
	})

	app.post('/updateGroupTemplates', function(require, response) {
		db.gallery.addNewTpl(require.body, function(result) {
			response.send(result);
		})
	})

	app.get('/getTpl/:templateId', function(req, res) {
		db.template.getTemplateCont(req.params.templateId, function(content) {
			res.send(content);
		});
	});

	app.post('/createAlbums', function(req, res, next) {
		db.template.getTemplateContent({
			templateId: req.body.templateId
		}, function(template) {
			var singleSuffix = req.body.children.length == 1
				? "/" + req.body.children[0].id
				: "";
			req.query.prefix = "users/" + req.body.userId + "/galleries/" + req.body.galleryId + singleSuffix;
			req.template = template;
			req.query.delimiter = "Thumb";
			req.commonPrefixes = true;
			next();
		});
	}, s3Api.s3ListObjects, function(req, res) {
		var mappedKeys = {};
		var mappedChildrenIds = {};
		req.body.children.forEach(function(child) {
			mappedChildrenIds[child.id] = true;
		});
		req.keys.forEach(function(keyObj) {
			var splitKeyObj = keyObj.Key.split("/");
			var childId = splitKeyObj[4];
			if (mappedChildrenIds[childId]) {
				var themeId = splitKeyObj[5];
				if (mappedKeys[childId]) {
					if (mappedKeys[childId][themeId]) {
						mappedKeys[childId][themeId].push(keyObj.Key);
					} else {
						mappedKeys[childId][themeId] = [keyObj.Key];
					}
				} else {
					mappedKeys[childId] = {};
					mappedKeys[childId][themeId] = [keyObj.Key];
				}
			}
		});
		req.body.children = req.body.children.map(function(child) {
			var childThemes = req.template.themes.map(function(theme) {
				var displayedPhotos = [];
				var themePhotos = mappedKeys[child.id]
					? mappedKeys[child.id][theme._id]
					: null;
				if (themePhotos) {
					displayedPhotos = mappedKeys[child.id][theme._id].slice(0, theme.design.coords.length);
				} else {
					displayedPhotos = [];
				}
				//push empty displayed photos so we'll place holders for all theme design coors
				while (displayedPhotos.length < theme.design.coords.length) {
					displayedPhotos.push("");
				}
				return {
					_id: theme._id,
					displayedPhotos: displayedPhotos.map(function(photo) {
						return {src: photo};
					}),
					design: theme._doc.design
				};
			});
			return {id: child.id, name: child.name, themes: childThemes}
		});

		db.album.createAlbums(req.body, function(albums) {
			if (!albums.error) {
				res.jsonp(albums);
			} else {
				console.log("error createAlbum: ", albums.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.post('/saveAlbum', function(req, res) {
		db.album.saveAlbum(req.body, function(album) {
			if (!album.error) {
				res.jsonp(album);
			} else {
				console.log("error saveAlbum: ", album.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.post('/updateAlbum', function(req, res) {
		db.album.updateAlbum(req.body, function(updatedAlbum) {
			if (!updatedAlbum.error) {
				res.jsonp(updatedAlbum);
			} else {
				console.log("error updateAlbum: ", updatedAlbum.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.post('/deleteAlbum', function(req, res) {
		db.album.deleteAlbum(req.body, function(deletedAlbum) {
			if (!deletedAlbum.error) {
				res.jsonp(deletedAlbum);
			} else {
				console.log("error deleteAlbum: ", deletedAlbum.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.get('/getAlbums', function(req, res) {
		db.album.getAlbums(req.query, function(albums) {
			res.send(albums);
		});
	});

	app.get('/getAlbumContent', function(req, res) {
		db.album.getAlbumContent(req.query, function(album) {
			res.send(album);
		});
	});

	app.post('/deletePhoto', function(req, res, next) {
		var thumbKey = req.body.thumb;
		var splitKey = thumbKey.split("/");
		var photoKey = "";
		splitKey.forEach(function(val, index) {
			if (index < splitKey.length - 2) {
				photoKey += val + "/";
			} else if (index == splitKey.length - 1) {
				photoKey += val.substr(0, val.lastIndexOf("Thumb"));
			}
		});
		req.keys = [
			{
				Key: photoKey
			}, {
				Key: thumbKey
			}
		];
		next();
	}, s3Api.deleteObjects, function(req, res) {
		res.jsonp({deleted: req.deleted});
	});

	app.post('/deleteChild', function(req, res, next) {
		db.gallery.deleteChild(req.body, function(deletedChild) {
			if (!deletedChild.error) {
				req.query = {
					prefix: "users/" + req.body.userId + "/galleries/" + req.body.galleryId + "/" + req.body.childId
				};
				next();
			} else {
				console.log("error deleteChild: ", deletedChild.error);
				res.status(400);
				res.send();
			}
		});
	}, s3Api.s3ListObjects, s3Api.deleteObjects, function(req, res) {
		res.jsonp({deleted: req.deleted});
	});

	app.post('/deleteGallery', function(req, res, next) {
		db.gallery.deleteGallery(req.body, function(deletedGallery) {
			if (!deletedGallery.error) {
				req.query = {
					prefix: "users/" + req.body.userId + "/galleries/" + req.body.galleryId
				};
				next();
			} else {
				console.log("error deleteGallery: ", deletedGallery.error);
				res.status(400);
				res.send();
			}
		});
	}, s3Api.s3ListObjects, s3Api.deleteObjects, function(req, res) {
		res.jsonp({deleted: req.deleted});
	});

	app.post('/deleteTemplate', function(req, res) {
		db.template.deleteTemplate(req.body, function(deletedTemplate) {
			if (!deletedTemplate.error) {
				res.jsonp(deletedTemplate);
			} else {
				console.log("error deleteTemplate: ", deletedTemplate.error);
				res.status(400);
				res.send();
			}
		});
	});

	app.post('/createPdf', function(req, res, next) {
		req.query = req.body;
		var cssPrefix = req.headers.origin + '/css/';
		var jsPrefix = req.headers.origin + '/js/';
		var prefixHtml = '<html><head>' +
		'<script src=\"' + jsPrefix + 'htmlUp/jquery.min.js"></script>' + '<script src=\"' + jsPrefix + 'canvasManipulator.js"></script>' + '<link rel="stylesheet" href=\"' + cssPrefix + 'skel.css" /><link rel="stylesheet" href=\"' + cssPrefix + 'style.css" /><link rel="stylesheet" href=\"' + cssPrefix + 'style-desktop.css" /><link rel="stylesheet" href=\"' + cssPrefix + 'general.css" /><link rel="stylesheet" href=\"' + cssPrefix + 'albumView.css" /></head><body>';
		var suffixHtml = '</body></html>';
		req.query.url = prefixHtml + req.query.url + suffixHtml;
		next();
	}, s3Api.sign_s3, function(req, res, next) {
		wkhtmltopdf(req.query.url, function(error) {
			if (error) {
				console.log("createPdf error: ", error);
			}
		}).pipe(accum(function(alldata) {
			// use the accumulated data - alldata will be Buffer, string, or []
			if (!alldata.length) {
				console.log("Error creating pdf from url: " + req.query.url);
				res.status(400);
				res.send();
			} else {
				req.binaryData = alldata;
				next();
			}
		}));
	}, s3Api.upload, function(req, res) {
		res.jsonp("ok!");
	});

	app.get('/auth/session', auth.ensureAuthenticated, session.session);
	app.post('/auth/session', session.login);
	app.delete('/auth/session', session.logout);

	app.get('/partials/*', function(req, res) {
		var requestedView = path.join('./', req.url);
		res.render(requestedView);
	});

	app.get('/*', function(req, res) {
		res.render('index.html');
	});
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}

function initFunc() {
	return true;
}
