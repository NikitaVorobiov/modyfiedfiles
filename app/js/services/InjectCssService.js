angular.module('createAlbumsApp').factory("injectCSS", ['$q', '$http', function($q){
    var injectCSS = {};
    var cssPrefix = "../css/";

    var createLink = function(id, url) {
        var link = document.createElement('link');
        link.id = id;
        link.rel = "stylesheet";
        link.type = "text/css";
        link.href = cssPrefix + url;
        return link;
    };

    var checkLoaded = function (url, deferred, tries) {
        for (var i in document.styleSheets) {
            var href = document.styleSheets[i].href || "";
            if (href.split("/").slice(-1).join() === url) {
                deferred.resolve();
                return;
            }
        }
        tries++;
        setTimeout(function(){checkLoaded(url, deferred, tries);}, 50);
    };

    injectCSS.set = function(cssUrls){
        var tries = 0,
            deferred = $q.defer(),
            link;

        angular.forEach(cssUrls, function(cssUrl){
            if(!angular.element('link#' + cssUrl.id).length) {
                link = createLink(cssUrl.id, cssUrl.url);
                link.onload = deferred.resolve;
                angular.element('head').append(link);
            }
            checkLoaded(cssUrl.url, deferred, tries);
        });

        return deferred.promise;
    };

    return injectCSS;
}]);