angular.module('createAlbumsApp')
  .factory('Session', function ($resource) {
    return $resource('/auth/session/');
  });
