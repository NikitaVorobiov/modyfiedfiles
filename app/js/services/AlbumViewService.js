angular.module('createAlbumsApp')
    .factory('AlbumView', function() {
        var galleryId, childId, templateId, albumName, themes;

        return {
            setGalleryId: function(_galleryId){
                galleryId = _galleryId;
            },
            getGalleryId: function(){
                return galleryId;
            },
            setChildId: function(_childId){
                childId = _childId;
            },
            getChildId: function(){
                return childId;
            },
            setTemplateId: function(_templateId){
                templateId = _templateId;
            },
            getTemplateId: function(){
                return templateId;
            },
            setAlbumName: function(_albumName){
                albumName = _albumName;
            },
            getAlbumName: function(){
                return albumName;
            },
            setThemes: function(_themes){
                themes = _themes;
            },
            getThemes: function(){
                return themes;
            }
        };
    });