angular.module('createAlbumsApp')
    .factory('Upload', function($http) {

        return {
            s3_upload: function(config){
                if(config.files.length){
                    var status_elem = document.getElementById("status");
                    new S3Upload({
                        path: config.path,
                        fileNames: config.fileNames,
                        files: config.files,
                        s3_sign_put_url: '/sign_s3',
                        onProgress: function(percent, message) {
                            status_elem.innerHTML = 'Upload progress: ' + percent + '% ' + message;
                        },
                        onFinishS3Put: function(public_url, file){
                            status_elem.innerHTML = 'Upload completed. Uploaded to: '+ public_url;
                            config.onFinishS3Put(public_url, file);
                        },
                        onError: function(status) {
                            status_elem.innerHTML = 'Upload error: ' + status;
                        }
                    });
                }
            },
            createFileNames : function(id, files){
                angular.forEach(files, function(file, index){
                    file.targetFileName = id + '_' + new Date().getTime() + '_' + index;
                });
            },
            resizeImage: function(resizeConfig, callback){
                $http({
                    method: "POST",
                    url:"resizeImage",
                    data: {
                        path: resizeConfig.public_url,
                        key: resizeConfig.resizePath,
                        s3_object_type: resizeConfig.fileType
                    }
                })
                    .success(function(data) {
                        if(data.resized){
                            var splitUrl = resizeConfig.public_url.split("/");
                            var key = resizeConfig.resizePath + splitUrl[splitUrl.length - 1] + "Thumb";
                            if(callback){
                                callback(key);
                            }
                        }
                    })
                    .error(function() {
                        alert("resize photo failed!");
                    });
            },
            splitImage: function(splitConfig){
                $http({
                    method: "POST",
                    url:"splitImage",
                    data: {
                        path: splitConfig.public_url,
                        key: splitConfig.path,
                        s3_object_type: splitConfig.fileType,
                        dim: splitConfig.dim,
                        gravity: splitConfig.gravity
                    }
                })
                    .success(function() {
                        console.log("split " + splitConfig.gravity);
                    })
                    .error(function() {
                        alert("split photo failed!");
                    });
            },
            rotateImage: function(rotateConfig, callback, errorCallback){
                $http({
                    method: "POST",
                    url:"rotateImage",
                    data: {
                        path: rotateConfig.public_url,
                        key: rotateConfig.rotatePath,
                        s3_object_type: rotateConfig.fileType,
                        rotateType: rotateConfig.rotateType
                    }
                })
                    .success(function(data) {
                        if(data.rotated){
                            var splitUrl = rotateConfig.public_url.split("/");
                            var key = rotateConfig.rotatePath + splitUrl[splitUrl.length - 1];
                            if(callback){
                                callback(key);
                            }
                        }
                    })
                    .error(function() {
                        alert("rotate photo failed!");
                        if(errorCallback){
                            errorCallback();
                        }
                    });
            }
        };
    });