angular.module('createAlbumsApp')
    .factory('General', function(usSpinnerService) {
        var spinnerActive = false;
        return {
            getCoordsFactor: function(){
                return {xFactor:1, yFactor:1};
            },
            getS3Host: function(){
                return "https://createalbums1.s3.amazonaws.com/";
            },
            toggleSpinner: function(){
                if (!spinnerActive) {
                    spinnerActive = true;
                    usSpinnerService.spin('spinner-1');
                } else {
                    spinnerActive = false;
                    usSpinnerService.stop('spinner-1');
                }
            }
        };
    });