
angular.module('createAlbumsApp').config( function( $compileProvider ) {
	$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|whatsapp):/);
})
