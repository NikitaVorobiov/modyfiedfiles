angular.module('createAlbumsApp')
    .factory('Gallery', function() {
        var gallery, child;

        return {
            getGallery: function(){
                return gallery;
            },
            setGallery: function(_gallery){
                gallery = _gallery;
            },
            setChild: function(_child){
                child = _child;
            },
            getChild: function(){
                return child;
            }
        };
    });