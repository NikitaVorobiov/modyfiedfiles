angular.module('createAlbumsApp')
    .factory('Templates', function($state, dialogs, $http) {
        var checkedThemes = [];
        var currentIndex = 0;
        var templateName = "";

        var clearTemplate = function(){
            checkedThemes = [];
            currentIndex = 0;
            templateName = "";
        };

        return {
            setCheckedThemes: function(_themes){
                checkedThemes = _themes;
            },
            getCheckedThemes: function(){
                return checkedThemes;
            },
            getCurrentTheme: function(){
                return checkedThemes[currentIndex];
            },
            incrementTheme: function(){
                ++currentIndex;
            },
            decrementTheme: function(){
                --currentIndex;
            },
            setThemeImage: function(imageData, id){
                angular.forEach(checkedThemes, function(theme, index) {
                    if(id == theme._id){
                        checkedThemes[index].imageData = imageData;
                    }
                });
            },
            isLastTheme: function(){
                return currentIndex + 1 == checkedThemes.length;
            },
            isFirstTheme: function(){
                return currentIndex == 0;
            },
            setTemplateName: function(_templateName){
                templateName = _templateName;
            },
            getTemplateName: function(){
                return templateName;
            },
            templateDialog: function(config, onSuccess){
                $state.go('create-template');
                var dlg = dialogs.create('/dialogs/createTemplateDialog.html', 'createTemplateDlgController',{}, {backdrop:'static'});
                dlg.result.then(function(){
                    var themes = checkedThemes.map(function(checkedTheme){
                        var imageData = {};
                        if(checkedTheme.imageData){
                            imageData = checkedTheme.imageData;
                        } else if(checkedTheme.designs.length){
                            imageData = {coords:checkedTheme.designs[0].coords, bg:""}
                        }
                        return {
                            _id: checkedTheme._id,
                            design: imageData
                        };
                    });
                    var data = angular.extend({name: templateName, themes: themes}, config.data);
                    $http({
                        method: "POST",
                        url: config.mode,
                        data: data
                    }).success(function(template) {
                        if(onSuccess){
                            onSuccess(template)
                        }
                    }).error(function() {
                        alert("Save template failed!");
                    });
                    $state.go('default-state');
                    clearTemplate();
                }, function(){
                    $state.go('default-state');
                    clearTemplate();
                });
            }
        };
    });