angular.module('createAlbumsApp')
    .factory('Auth', function Auth($location, $rootScope, Session, $cookieStore, $q) {
        $rootScope.currentUser = $cookieStore.get('user') || null;
        $cookieStore.remove('user');
        var promise;
        return {
            login: function (provider, user, callback) {
                var cb = callback || angular.noop;
                Session.save({
                    provider: provider,
                    email: user.email,
                    password: user.password,
                    rememberMe: user.rememberMe
                }, function (user) {
                    $rootScope.currentUser = user;
                    return cb();
                }, function (err) {
                    return cb(err.data);
                });
            },

            logout: function (callback) {
                var cb = callback || angular.noop;
                Session.delete(function () {
                        $rootScope.currentUser = null;
                        return cb();
                    },
                    function (err) {
                        return cb(err.data);
                    });
            },

            currentUser: function () {
                if (!promise) {
                    promise = $q(function (resolve) {
                        Session.get(function (user) {
                            $rootScope.currentUser = user;
                            resolve(true);
                        }, function () {
                            resolve(false)
                        });
                    });
                }
                return promise;
            },

            isCurrentUser: function () {
                if (!$rootScope.currentUser) {
                    $location.path("/login");
                }
            }
        };
    });