window.onload = function() {
    var $ = jQuery;
    var albumEl = $(".album-view-container");
    albumEl.find(".change-photo, .arrow, .buttons-container, .nav").remove();
    albumEl.find(".album-view").addClass("pdfMode");
    var siteWidth = 435;
    var siteHeight = 300;
    $.each(albumEl.find(".theme-album"), function(index, theme){
        var bgHeight = 1416;
        var bgWidth = 1000;
        var $theme = $(theme);
        var leftBgImg = $theme.find(".theme-bg-image.left");
        var rightBgImg = $theme.find(".theme-bg-image.right");
        var leftBgWrapper = $("<div>").addClass("images-wrapper").prependTo($theme);
        var rightBgWrapper = $("<div>").addClass("images-wrapper").prependTo($theme);
        leftBgImg.appendTo(leftBgWrapper);
        rightBgImg.appendTo(rightBgWrapper);
        leftBgImg.height(bgHeight);
        leftBgImg.width(bgWidth);
        rightBgImg.height(bgHeight);
        rightBgImg.width(bgWidth);
        //draw images
        var xFactor = (bgWidth*2)/siteWidth;
        var yFactor = bgHeight/siteHeight;
        var images = $theme.find(".image-wrapper");
        $.each(images ,function(index, imageWrapper){
            var $imageWrapper = $(imageWrapper);
            var image = $imageWrapper.find(".image");
            if(!image[0].src){  //remove blank images
                image.remove();
            } else {
                var left = parseInt($imageWrapper.css("left"));
                var top = parseInt($imageWrapper.css("top"));
                $imageWrapper.width(100*xFactor);
                $imageWrapper.height(100*yFactor);
                image.css("maxWidth", 100*xFactor + "px");
                image.css("maxHeight", 100*yFactor + "px");
                $imageWrapper.css("top", parseInt($imageWrapper.css("top"))*yFactor);
                if(left < siteWidth/2){
                    leftBgWrapper.append($imageWrapper);
                    $imageWrapper.css("left", parseInt($imageWrapper.css("left"))*xFactor);
                } else {
                    rightBgWrapper.append($imageWrapper);
                    $imageWrapper.css("left", (parseInt($imageWrapper.css("left")) - siteWidth/2)*xFactor);
                }
            }
        });

    });
};