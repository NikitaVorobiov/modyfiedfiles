angular.module('createAlbumsApp')
    .controller('galleryDlgController',function($scope,$modalInstance, Gallery){
        //-- Variables --//

        var galleryName = "";
        $scope.title = "Create new gallery";
        var gallery = Gallery.getGallery();
        if(gallery){
            galleryName = gallery.name;
            $scope.title = "Update gallery";
        }
        $scope.newGallery = {name:galleryName, children:[]};

        //-- Methods --//

        $scope.cancel = function(){
            $modalInstance.dismiss('Canceled');
        }; // end cancel

        $scope.save = function(){
            $modalInstance.close($scope.newGallery);
        }; // end save

        $scope.hitEnter = function(evt){
            if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.newGallery.name,null) || angular.equals($scope.newGallery.galleryName,'')))
                $scope.save();
        };

        $scope.nameCount = 0;
        $scope.onBlur = function(index){
            console.log("index = ", index);
        };
    })
    .controller('uploadDlgController',function($scope,$modalInstance, data, FileUploader){
        $scope.type = data.type == "folder" ? "folder" : "files";

        var uploader = $scope.uploader = new FileUploader({
            path: data.path,
            createFileNames: data.createFileNames,
            onFinishS3Put: data.onFinishS3Put,
            getPath: data.getPath,
            onSelect: data.onSelect
        });

        // FILTERS

        uploader.filters.push({
            name: 'typeFilter',
            fn: function(item) {
                return item.type.indexOf("image") == 0;
            }
        });

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('Canceled');
        }; // end cancel

        $scope.save = function(){
            $modalInstance.close($scope.newGallery);
        }; // end save
    })
    .controller('replacePhotoDlgController',function($scope, $modalInstance, data, General){
        $scope.photos = data.photos;
        $scope.selectedPhoto = 0;
        $scope.host = General.getS3Host();

        $scope.cancel = function(){
            $modalInstance.dismiss('Canceled');
        }; // end cancel

        $scope.replace = function(){
            $modalInstance.close({index:data.index, selectedPhoto: $scope.selectedPhoto});
        }; // end save

        $scope.selectPhoto = function(item, index){
            $scope.selectedPhoto = index;
        };

        $scope.isActive = function (index) {
            return $scope.selectedPhoto === index;
        };
    })
    .controller('editPhotoDlgController',function($scope,$modalInstance, data, General){
        $scope.photo = data.photo;
        $scope.host = General.getS3Host();

        $scope.cancel = function(){
            $modalInstance.dismiss('Canceled');
        }; // end cancel

        $scope.edit = function(){
            $modalInstance.close($scope.getFilters());
        }; // end save
    })
    .controller('createTemplateDlgController',function($scope,$modalInstance, data, $state){
        $scope.disabledState = "create-template";
        console.log(data);
        $scope.state = data;
        $scope.isDisabled = function(){
            return $state.current.name != "template-theme-last";
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('Canceled');
        }; // end cancel

        $scope.save = function(){
            $modalInstance.close({});
        }; // end save
    })
    .controller('albumLinkDlgController',function($scope,$modalInstance, data){
        $scope.link = data.link;

        // $scope.close = function(){
        //     $modalInstance.close({});
        // };
    })
    .run(['$templateCache',function($templateCache){
        $templateCache.put('/dialogs/galleryDialog.html',
            '<div class="modal-header">' +
                '<h4 class="modal-title"><span class="glyphicon glyphicon-star"></span> {{title}}</h4>' +
            '</div>' +
            '<div class="gallery-dialog modal-body">' +
                '<ng-form name="galleryDialog" novalidate role="form">' +
                    '<div class="form-group input-group-lg" ng-class="{true: \'has-error\'}[galleryDialog.$dirty && galleryDialog.$invalid]">' +
                        '<label class="control-label" for="course">Gallery Name: </label>' +
                        '<input type="text" class="form-control" ng-model="newGallery.name" ng-keyup="hitEnter($event)" required>' +
                    '</div>' +
                    '<div>' +
                '<addnamesbutton></addnamesbutton>' +
                '<div id="space-for-names"></section>' +
                    '</div>' +
                '</ng-form>' +
            '</div>' +
            '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>' +
                '<button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="!newGallery.name">Save</button>' +
            '</div>');

        $templateCache.put('/dialogs/uploadDialog.html',
            '<div class="modal-header">' +
                '<h4 class="modal-title"><span class="glyphicon glyphicon-star"></span>Upload</h4>' +
            '</div>' +
            '<div class="col-md-9 upload-dialog" style="margin-bottom: 40px">' +
                '<p class="queue-length">Queue length: {{ uploader.queue.length }}</p>' +
                '<table class="table">'+
                    '<thead class="upload-table-head">'+
                        '<tr>'+
                            '<th>Name</th>'+
                            '<th ng-show="uploader.isHTML5">Size</th>'+
                            '<th ng-show="uploader.isHTML5">Progress</th>'+
                            '<th>Status</th>'+
                            '<th>Actions</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody class="upload-table-body">'+
                        '<tr ng-repeat="item in uploader.queue">' +
                            '<td><strong>{{ item.file.name }}</strong></td>'+
                            '<td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>'+
                            '<td ng-show="uploader.isHTML5">'+
                                '<div class="progress" style="margin-bottom: 0;">'+
                                    '<div class="progress-bar" role="progressbar" ng-style="{ width: item.progress + \'%\' }"></div>'+
                                '</div>'+
                            '</td>'+
                            '<td class="text-center">'+
                                '<span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>'+
                                '<span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>'+
                                '<span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>'+
                            '</td>'+
                            '<td nowrap>'+
                                '<button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">'+
                                    '<span class="glyphicon glyphicon-upload"></span> Upload'+
                                '</button>'+
                                '<button type="button" class="btn btn-warning btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">'+
                                    '<span class="glyphicon glyphicon-ban-circle"></span> Cancel'+
                                '</button>'+
                                '<button type="button" class="btn btn-danger btn-xs" ng-click="item.remove()">'+
                                    '<span class="glyphicon glyphicon-trash"></span> Remove'+
                                '</button>'+
                            '</td>'+
                        '</tr>'+
                    '</tbody>'+
                '</table>'+

                '<div>'+
                    '<div>'+
                    'Queue progress:'+
                        '<div class="progress" style="">'+
                            '<div class="progress-bar" role="progressbar" ng-style="{ width: uploader.progress + \'%\' }"></div>'+
                        '</div>'+
                    '</div>'+
                    '<button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">'+
                        '<span class="glyphicon glyphicon-upload"></span> Upload all'+
                    '</button>'+
                    '<button type="button" class="btn btn-warning btn-s" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">'+
                        '<span class="glyphicon glyphicon-ban-circle"></span> Cancel all'+
                    '</button>'+
                    '<button type="button" class="btn btn-danger btn-s" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">'+
                        '<span class="glyphicon glyphicon-trash"></span> Remove all'+
                    '</button>'+
                    '<button class="choose-files-btn btn btn-s btn-primary" onclick="document.getElementById(\'choose-button\').click()">Choose {{type}}</button>'+
                    '<div ng-switch="type">'+
                        '<input ng-switch-when="files" id="choose-button" nv-file-select="" ng-show=false type="file" uploader="uploader" multiple />'+
                        '<input ng-switch-when="folder" id="choose-button" nv-file-select="" ng-show=false type="file" uploader="uploader" multiple webkitdirectory directory/>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>' +
                '<button type="button" class="btn btn-primary" ng-click="save()">Ok</button>' +
            '</div>'
        );
        $templateCache.put('/dialogs/replacePhotoDialog.html',
            '<div class="modal-header">' +
                '<h4 class="modal-title"><span class="glyphicon glyphicon-star"></span>Replace photo</h4>' +
            '</div>' +
            '<div>'+
                '<thumbs-list items="photos" on-click="selectPhoto(item,index)" active="selectedPhoto"></thumbs-list>'+
            '</div>'+
            '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>' +
                '<button type="button" class="btn btn-primary" ng-click="replace()">Ok</button>' +
            '</div>'

        );
        $templateCache.put('/dialogs/editPhotoDialog.html',
                '<div class="modal-header">' +
                    '<h4 class="modal-title"><span class="glyphicon glyphicon-star"></span>Edit photo</h4>' +
                '</div>' +
                '<div>'+
                    '<css-editor></css-editor>'+
                '</div>'+
                '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>' +
                    '<button type="button" class="btn btn-primary" ng-click="edit()">Ok</button>' +
                '</div>'

        );
        $templateCache.put('/dialogs/createTemplateDialog.html',
                '<div class="modal-header">' +
                    '<h4 class="modal-title"><span class="glyphicon glyphicon-star"></span>Create template</h4>' +
                '</div>' +
                '<div ui-view class="template-dialog-body">'+
                '</div>'+
                '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>' +
                    '<button ng-disabled="isDisabled()" type="button" class="btn btn-primary" ng-click="save()">Ok</button>' +
                '</div>'

        );
        $templateCache.put('/dialogs/albumLinkDialog.html',
                '<div class="modal-header">' +
                    '<h4 class="modal-title"><span class="glyphicon glyphicon-star"></span>Album link</h4>' +
                '</div>' +
                '<div class="album-link-dialog-body">'+
                    '<div>{{link}}</div>'+
                '</div>'+
                '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-primary" ng-click="close()">Ok</button>' +
                '</div>'

        );
    }]);
