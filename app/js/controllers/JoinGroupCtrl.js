(function() {

	'use strict';

	angular.module('createAlbumsApp').controller("JoinGroupCtrl", function($scope, $http, $rootScope, $location, $state) {
		var thisLocation = $location.absUrl();
		var thisGroupId = thisLocation.substring(thisLocation.indexOf('joinGroup/') + 10, thisLocation.indexOf('joinGroup/') + 34);
		var newUserType = thisLocation.substring(thisLocation.length - 2);

		$scope.userType = {
			admin: false,
			professional: false,
			member: false
		};

		$scope.newChildInput = '';
		$scope.checkedChildren = [];

		$http.get('/getGroups/' + thisGroupId).then(function success(group) {
			$scope.group = group.data[0];
			$scope.children = $scope.group.children;

		}, function error(err) {
			console.log(err);
		});

		if (newUserType === 'a0') {
			$scope.userType.admin = true;
		} else if (newUserType === 'p1') {
			$scope.userType.professional = true;
		} else {
			$scope.userType.member = true;
		};

		var checkThisUser = function(group, success) {
			var checkResult = false;
			for (var i = 0; i < group.members.length; i++) {
				if (group.members[i].memberId === $rootScope.currentUser._id) {
					checkResult = true;
					break;
				}
			};

			if (!checkResult) {
				success()
			} else {
				alert('You are already in this group');
				$location.path('/galleries');
			}
		};

		$scope.parentGender = '';
		$scope.joinNewParent = function(children) {
			if ($scope.parentGender !== '') {
				for (var i = 0; i < children.length; i++) {
					children[i][$scope.parentGender] = {
						email: $rootScope.currentUser.email,
						name: $rootScope.currentUser.firstName,
						phone: $rootScope.currentUser.phone || 'phone number'
					}
				};

				$http.get('/getGroups/' + thisGroupId).then(function success(res) {
					checkThisUser($scope.group, function() {
						updateGroupMembers({
							_id: thisGroupId,
							memberInfo: {
								type: 'parent',
								children: children,
								memberId: $rootScope.currentUser._id,
								memberName: $rootScope.currentUser.firstName
							}
						});

						updateUserGroups({
							userId: $rootScope.currentUser._id,
							groupInfo: {
								groupId: $scope.group._id,
								groupName: $scope.group.name
							}
						});

						$location.path('/galleries');
					});
				}, function error(err) {
					console.log(err);
				});

			} else {
				alert('You need to choose your gender type');
			}
	};

	$scope.joinNewMember = function(userType) {
		$http.get('/getGroups/' + thisGroupId).then(function success(group) {
			checkThisUser($scope.group, function() {

				updateGroupMembers({
					_id: thisGroupId,
					memberInfo: {
						type: userType,
						memberId: $rootScope.currentUser._id,
						memberName: $rootScope.currentUser.firstName
					}
				});

				updateUserGroups({
					userId: $rootScope.currentUser._id,
					groupInfo: {
						groupId: $scope.group._id,
						groupName: $scope.group.name
					}
				});

				$location.path('/galleries');
			})

		}, function error(err) {
			console.log(err);
		});
	};

	var updateGroupMembers = function(userInfo) {
		$http.post('/updateGroupMembers', userInfo).then(function success(res) {
			// console.log(res);
		}, function error(err) {
			// console.log(err);
		});
	};

	var updateUserGroups = function(groupInfo) {
		console.log(groupInfo);
		$http.post('/updateUserGroups', groupInfo).then(function success(res) {
			console.log(res);
		}, function error(err) {
			console.log(err);
		});
	};

});
})();
