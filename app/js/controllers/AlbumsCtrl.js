angular.module('createAlbumsApp').controller('AlbumsController', function($scope, $rootScope, $location, $http, AlbumView, dialogs) {
	$scope.templates = [];
	$scope.title = "My albums";

	$http.get('getAlbums?userId=' + $rootScope.currentUser._id).success(function(albums) {
		if(!albums.error){
			console.log(albums);
			$scope.albums = albums;
		} else {
			alert(albums.error);
		}
	});

	$scope.openAlbum = function(album){
		AlbumView.setGalleryId(album.galleryId);
		AlbumView.setChildId(album.childId);
		AlbumView.setTemplateId(album.templateId);
		angular.forEach(album.themes, function(theme){
			theme.isAlbum = true;
		});
		AlbumView.setThemes(album.themes);
		AlbumView.setAlbumName(album.name);
		$location.path('/albumView/' + album._id);
	};

	var deleteAlbum = function(obj){
		var dlg = dialogs.confirm('Confirm delete', 'Are you sure you want to delete this album?', {backdrop:'static'});
		dlg.result.then(function(){
			$http({
				method: "POST",
				url:"deleteAlbum",
				data: {_id: obj.item._id}
			})
			.success(function(data) {
				if(data.deleted){
					$scope.albums.splice(obj.index, 1);
				}
			})
			.error(function() {
				alert("Delete album failed!");
			});
		});
	};

	$scope.menuItems = [
		{name:"Delete", func: deleteAlbum}
	];
});
