angular.module('createAlbumsApp').controller('RegisterController', function($scope, $rootScope, $http, $location) {

		$scope.labelInputs = [];
		$scope.firstName = {
				label: "First name",
				value: ""
		};
		$scope.lastName = {
				label: "Last name",
				value: ""
		};
		$scope.email = {
				label: "Email",
				value: ""
		};
		$scope.password = {
				label: "Password",
				value: ""
		};
		$scope.verifyPassword = {
				label: "Verify password",
				value: ""
		};
		$scope.labelInputs.push($scope.firstName);
		$scope.labelInputs.push($scope.lastName);
		$scope.labelInputs.push($scope.email);
		$scope.labelInputs.push($scope.password);
		$scope.labelInputs.push($scope.verifyPassword);

		$scope.newUserInfo = {};
		$scope.newUserInfo.sysType = "simpleUser";
		$scope.register = function() {

			if ($scope.newUserInfo.firstName === '' || $scope.newUserInfo.lastName === '') {
				alert('First/Last name must be biggest ore equal then 0');
			} else if($scope.newUserInfo.password !== $scope.newUserInfo.verifyPassword || $scope.newUserInfo.password.length < 8) {
				alert('Password field and verify field must be equal and your password must be biggest then 8 characters')
			} else {
				var responsePromise = $http({
						method: "POST",
						url: "createUser",
						data: {
								firstName: $scope.newUserInfo.firstName,
								lastName: $scope.newUserInfo.lastName,
								email: $scope.newUserInfo.email,
								password: $scope.newUserInfo.password,
								sysType: $scope.newUserInfo.sysType
						}
				});

				responsePromise.success(function(data) {
						data.user.sysType = $scope.newUserInfo.sysType;
						if (data.info) {
								alert(data.info.message);
						} else {
								$rootScope.currentUser = data.user;
								alert("Register succeeded");
								console.log($rootScope.currentUser);
								$location.path("/");
						}
				});
				responsePromise.error(function() {
						alert("Register failed!");
				});
			}
		};
});
