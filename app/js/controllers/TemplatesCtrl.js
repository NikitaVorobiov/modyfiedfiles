angular.module('createAlbumsApp').controller('TemplatesController', function($scope, $rootScope, $location, $http, dialogs, $state, Templates) {
	$scope.templates = [];
	$scope.title = "My templates";


	$scope.newTemplate = {};

	$scope.getNewTemplate = function getNewTemplate () {
		$http.post('/createTemplate', {
			picbuzzId: $scope.newTemplate.id,
			name: $scope.newTemplate.name,
			phLimit: $scope.newTemplate.phLimit,
			designerId: $scope.newTemplate.designerId
		}).then(function success (res) {
			console.log(res);
		}, function error (err) {
			console.log(err);
		});
	}


	// $scope.initNewTemplate = function initNewTemplate () {
	//
	// }


// 	$http.get('getTemplates?userId=' + $rootScope.currentUser._id).success(function (templates) {
// 		if(!templates.error){
// 			$scope.templates = templates;
// 		} else {
// 			alert(templates.error);
// 		}
// 	});
//
//
//
// 	$scope.createTemplate = function () {
// 		Templates.templateDialog({mode:"createTemplate", data:{userId: $rootScope.currentUser._id}}, function(template){
// 			console.log(template);
// 			$scope.templates.push(template);
// 		});
// 	};
//
// 	$scope.openTemplate = function(template){
// 		$location.path('/templates/' + template._id);
// 	};
//
// 	var deleteTemplate = function(obj){
// 		var dlg = dialogs.confirm('Confirm delete', 'Are you sure you want to delete this template?', {backdrop:'static'});
// 		dlg.result.then(function(){
// 			$http({
// 				method: "POST",
// 				url:"deleteTemplate",
// 				data: {_id: obj.item._id}
// 			})
// 			.success(function(data) {
// 				if(data.deleted) {
// 					$scope.templates.splice(obj.index, 1);
// 				}
// 			})
// 			.error(function() {
// 				alert("Delete template failed!");
// 			});
// 		});
// 	};
//
// 	$scope.menuItems = [
// 		{name:"Delete", func: deleteTemplate}
// 	];
// });
//
// angular.module('createAlbumsApp').controller('TemplateController', function($scope, $rootScope, $location, $http, dialogs, General, Templates, $routeParams, $state) {
// 	$scope.host = General.getS3Host();
//
// 	var templateId = $routeParams.template;
// 	if(templateId){
// 		$http.get('getTemplateContent/' + templateId).success(function(template) {
// 			if(!template.error){
// 				$scope.template = template;
// 				$scope.currentTheme = $scope.template.themes[$scope._Index];
// 				drawCoords();
// 				$scope.checkUserRights($scope.template);
// 				$scope.sendEmailForm = false;
//
// 				$scope.sendEmailInfo = {
// 					html: "This is link to " + $scope.template.name + '. Follow this link : <a href="' + $scope.templateLink + '"> Link </a>',
// 					subject: 'Template'
// 				}
// 				$scope.sendTemplateLink = function sendTemplateLink () {
// 					$http.post('/sendEmail', $scope.sendEmailInfo)
// 						.then(function success (result) {
// 							console.log(result.data)
// 							$scope.sendEmailForm = false;
// 						}, function error (err) {
// 							console.log(err);
// 						});
// 				};
// 			} else {
// 				alert(template.error);
// 			}
// 		});
// 	} else {
// 		$location.path('/templates');
// 	}
//
// 	$scope.templateLink = $location.absUrl();
//
// 	$scope.checkUserRights = function checkUserRights (tpl) {
// 		if (tpl.userId === $rootScope.currentUser._id) {
// 			$scope.isItAuthor = true;
// 		} else {
// 			$scope.isItAuthor = false;
// 			$http.get('/getUser/' + $rootScope.currentUser._id)
// 				.then(function success (user) {
// 					$scope.guestUser = user.data;
// 					$scope.groupInfo = {
// 						groupId: null,
// 						templateInfo: {
// 							templateId: $scope.template._id,
// 							templateName: $scope.template.name
// 						}
// 					};
// 					$scope.getTemplateForGroup = function getTemplateForGroup () {
// 						if ($scope.groupInfo.groupId === null) {
// 							alert('Please, select on of the group');
// 						} else {
// 							$http.post('/updateGroupTemplates', $scope.groupInfo)
// 								.then(function success (result) {
// 									console.log(result);
// 									$location.path('/galleries');
// 								}, function error (err) {
// 									console.log(err);
// 								})
// 						}
// 					};
// 				}, function error (err) {
// 					console.log(err);
// 				});
// 		}
// 	}
//
// 	var drawCoords = function(){
// 		var coords = $scope.currentTheme.design.coords.map(function(coord){
// 			var coordsFactor = General.getCoordsFactor();
// 			return {x:coord.x*coordsFactor.xFactor, y:coord.y*coordsFactor.yFactor};
// 		});
// 		$scope.onSelect(coords);
// 	};
//
// 	// initial image index
// 	$scope._Index = 0;
//
// 	// if a current image is the same as requested image
// 	$scope.isActive = function (index) {
// 		return $scope._Index === index;
// 	};
//
// 	// show prev image
// 	$scope.showPrev = function () {
// 		if($scope._Index == 0){
// 			return;
// 		}
// 		$scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.template.themes.length - 1;
// 		$scope.currentTheme = $scope.template.themes[$scope._Index];
// 		drawCoords();
// 	};
//
// 	// show next image
// 	$scope.showNext = function () {
// 		if($scope._Index == $scope.template.themes.length - 1){
// 			return;
// 		}
// 		$scope._Index = ($scope._Index < $scope.template.themes.length - 1) ? ++$scope._Index : 0;
// 		$scope.currentTheme = $scope.template.themes[$scope._Index];
// 		drawCoords();
// 	};
//
// 	// show a certain theme
// 	$scope.jumpToTheme = function (index) {
// 		$scope._Index = index;
// 		$scope.currentTheme = $scope.template.themes[$scope._Index];
// 		drawCoords();
// 	};
//
// 	$scope.editTemplate = function(){
// 		$state.go('create-template');
// 		Templates.setCheckedThemes($scope.template.themes);
// 		Templates.setTemplateName($scope.template.name);
// 		Templates.templateDialog({mode:"updateTemplate", data:{_id: $scope.template._id}}, function(template){
// 			$scope.template = template;
// 		});
// 	};
}).config(['$compileProvider',
  function($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|whatsapp):/);
  }
]);
