angular.module('createAlbumsApp').controller('GalleriesController', function($scope, $rootScope, $location, dialogs, $http, Gallery, Upload) {
	$scope.title = "My galleries";
	Gallery.setGallery(null);
	console.log($rootScope.currentUser);
	$scope.galleries = [];
	$http.get('/getUser/' + $rootScope.currentUser._id).then(function success(user) {
		user.data.groups.forEach(function(group) {
			$http.get('/getGroups/' + group.groupId).then(function success(gallery) {
				$scope.galleries.push(gallery.data[0]);
			}, function error(err) {
				console.log(err);
			})
		})
	}, function error(err) {
		console.error(err);
	});
	$scope.createGallery = function() {
		var dlg = dialogs.create('/dialogs/galleryDialog.html', 'galleryDlgController', {}, {backdrop: 'static'});
		dlg.result.then(_createGallery);
	};

	var _createGallery = function(newGallery, callback) {
		$http({
			method: "POST",
			url: "createGallery",
			data: {
				userId: $rootScope.currentUser._id,
				children: newGallery.children,
				name: newGallery.name,
				memberInfo: {
					type: 'admin',
					memberId: $rootScope.currentUser._id,
					memberName: $rootScope.currentUser.firstName
				}
			}
		}).success(function(gallery) {
			console.log('New Group ==>' + $scope.galleries);
			$scope.galleries.push(gallery);
			if (callback) {
				callback(gallery);
			};
			$http({
				method: "POST",
				url: "/updateUserGroups",
				data: {
					userId: $rootScope.currentUser._id,
					groupInfo: {
						groupId: gallery._id,
						groupName: gallery.name
					}
				}
			}).success(function(res) {
				console.log(res);
			}).error(function(err) {
				console.log(err);
			});

		}).error(function() {
			alert("Create gallery failed!");
		});
	};

	var parseFolders = function(files, changeOrder) {
		var tree = {};
		var invalid = [];
		var numOfValid = 0;
		angular.forEach(files, function(file) {
			if (file.type.indexOf("image") == 0) {
				var pathParts = file.webkitRelativePath.split('/');
				if (pathParts.length != 4) {
					invalid.push(file.webkitRelativePath);
				} else {
					if (changeOrder) {
						var temp = pathParts[1];
						pathParts[1] = pathParts[2];
						pathParts[2] = temp;
					}
					var subObj = tree;
					angular.forEach(pathParts, function(folderName, index) {
						if (!subObj[folderName]) {
							if (index < pathParts.length - 1) {
								subObj[folderName] = {};
							} else {
								subObj[folderName] = file;
								numOfValid++;
							}
						}
						subObj = subObj[folderName];
					});
				}
			}
		});
		return {tree: tree, invalid: invalid, numOfValid: numOfValid};
	};

	$scope.folderUpload = function(files, changeOrder, callback, uploadCallback) {
		var parsedFolders = parseFolders(files, changeOrder);
		if (parsedFolders.invalid.length) {
			alert("Some of the files have invalid folder structure. For more info check the browser's console.");
			console.log("Invalid folder structure for the following files: ");
			angular.forEach(parsedFolders.invalid, function(fileName) {
				console.log(fileName);
			});
		}
		if (!parsedFolders.numOfValid) {
			uploadCallback(false);
		} else {
			var galleryName = Object.keys(parsedFolders.tree)[0];
			var children = [];
			angular.forEach(parsedFolders.tree[galleryName], function(childObj, childName) {
				children.push({
					name: childName,
					mother: {
						name: "",
						email: "",
						phone: ""
					},
					father: {
						name: "",
						email: "",
						phone: ""
					},
					photos: []
				});
			});
			var gallery = {
				name: galleryName,
				children: children
			};
			_createGallery(gallery, function(newGallery) {
				var fileToPath = {};
				getOrCreateThemes(newGallery.children, parsedFolders.tree[galleryName], function(themesMap) {
					if (!themesMap) {
						uploadCallback(false);
					} else {
						angular.forEach(newGallery.children, function(child) {
							var parsedThemes = parsedFolders.tree[galleryName][child.name];
							var themesNames = Object.keys(parsedThemes);
							angular.forEach(themesNames, function(themeName) {
								var parsedTheme = parsedThemes[themeName];
								var themeFiles = Object.keys(parsedTheme).map(function(key) {
									return parsedTheme[key]
								});
								var themeId = themesMap[themeName]._id;
								var path = 'users/' + $rootScope.currentUser._id + '/galleries/' + newGallery._id + '/' + child._id + '/' + themeId + '/';
								angular.forEach(themeFiles, function(themeFile) {
									fileToPath[themeFile.webkitRelativePath] = path;
								});
								callback({id: themeId, fileToPath: fileToPath, themeFiles: themeFiles});
							});
						});
						uploadCallback(true);
					}
				});
			});
		}
	};

	var getOrCreateThemes = function(children, themesTree, callback) {
		var mappedThemesNames = {};
		var themesNames = [];
		angular.forEach(children, function(child) {
			var parsedChildThemes = themesTree[child.name];
			angular.forEach(Object.keys(parsedChildThemes), function(childThemeName) {
				if (!mappedThemesNames[childThemeName]) {
					mappedThemesNames[childThemeName] = true;
					themesNames.push(childThemeName);
				}
			});

		});
		$http({
			method: "POST",
			url: "getOrCreateThemes",
			data: {
				userId: $rootScope.currentUser._id,
				themesNames: themesNames
			}
		}).success(function(themes) {
			var themesMap = {};
			angular.forEach(themes, function(theme) {
				themesMap[theme.name] = theme;
			});
			callback(themesMap);
		}).error(function() {
			alert("Error in creating or getting themes");
			callback(false)
		});
	};

	$scope.s3_upload = function(changeOrder) {
		var fileToPath = {};
		var config = {
			onFinishS3Put: function(public_url, file) {
				var resizePath = fileToPath[file.webkitRelativePath] + 'thumbs/';
				Upload.resizeImage({public_url: public_url, resizePath: resizePath, fileType: file.type});
			},
			createFileNames: function(files) {},
			path: null,
			type: "folder",
			onSelect: function(files, callback) {
				$scope.folderUpload(files, changeOrder, function(data) {
					Upload.createFileNames(data.id, data.themeFiles);
					angular.extend(fileToPath, data.fileToPath);
				}, function(success) {
					callback(success);
				});
			},
			getPath: function(file) {
				return fileToPath[file.webkitRelativePath];
			}
		};
		dialogs.create('/dialogs/uploadDialog.html', 'uploadDlgController', config, {backdrop: 'static'});
	};

	$scope.openGallery = function(gallery) {
		$location.path('/galleries/' + gallery._id);
	};

	var deleteGallery = function(obj) {
		var dlg = dialogs.confirm('Confirm delete', 'Are you sure you want to delete this gallery?', {backdrop: 'static'});
		dlg.result.then(function() {
			$http({
				method: "POST",
				url: "deleteGallery",
				data: {
					userId: $rootScope.currentUser._id,
					galleryId: obj.item._id
				}
			}).success(function(data) {
				if (data.deleted >= 0) {
					$scope.galleries.splice(obj.index, 1);
				}
			}).error(function() {
				alert("delete gallery failed!");
			});
		});
	};

	$scope.menuItems = [
		{
			name: "Delete",
			func: deleteGallery
		}
	];
});

angular.module('createAlbumsApp').controller('GalleryController', function($scope, $rootScope, $location, $http, dialogs, Gallery, $routeParams, $state) {
	var galleryId = $routeParams.gallery;

	$scope.goToTemplates = function goToTemplates() {
		$location.path('/groupTemplates/' + galleryId);
	};

	var checkUserType = function (group) {
		for (var i = 0; i < group.members.length; i++) {
			if (group.members[i].memberId === $rootScope.currentUser._id) {
				$rootScope.currentUser.memberType = group.members[i].type + '';
				console.log($rootScope.currentUser.memberType);
				$scope.rights = {
					admin: false,
					professional: false,
					parent: false
				};

				if ($rootScope.currentUser.memberType === 'admin') {
					$scope.rights.admin = true;
				} else if ($rootScope.currentUser.memberType === 'professional') {
					$scope.rights.professional = true;
				} else {
					console.log($rootScope.currentUser.memberType);
					$scope.rights.member = true;
				};

			}
		};
	};
	console.log(Gallery);
	$scope.trueInvite = false;

	if (galleryId) {
		$http.get('getGalleryContent?galleryId=' + galleryId).success(function(gallery) {
			if (!gallery.error) {
				$scope.title = gallery.name + " Group";
				$scope.gallery = gallery;
				$scope.showInvite = function (typeKey) {
					$scope.domen = $location.absUrl().substring(0, $location.absUrl().indexOf('/gall'));
					$scope.inviteHash = gallery._id + typeKey;
					$scope.trueInvite = false;
					$scope.showLink = true;
					$scope.showLinkToggle = true;
				};
				checkUserType($scope.gallery);
				Gallery.setGallery(gallery);
				$scope.sendEmail = function () {
					$scope.domen = $location.absUrl().substring(0, $location.absUrl().indexOf('/gall'));
					$scope.inviteLink = $scope.domen + '/joinGroup/' + gallery._id + $scope.recipientType
					$http({
						method: "POST",
						url: "sendEmail",
						data: {
							recipient: $scope.recipientEmail,
							html: '<p>Hello, you was invited in ' + gallery.name + 'Follow this link, to join this group : </p> <a href="' + $scope.inviteLink + ' ">' + gallery.name + '</a>',
							subject: 'Invite'
							// groupName: gallery.name,
							// joinLink: $scope.domen + '/joinGroup/' + gallery._id + $scope.recipientType
						}
					}).success(function (result) {
						console.log(result);
					}).error(function (err) {
						console.log(err);
					});
				};
			} else {
				alert(gallery.error);
			}
		});
	} else {
		$location.path('/galleries');
	};

	$scope.addToGallery = function addToGallery () {
		var dlg = dialogs.create('/dialogs/galleryDialog.html', 'galleryDlgController', {}, {backdrop: 'static'});
		dlg.result.then(function(newGallery) {
			$http({
				method: "POST",
				url: "updateGallery",
				data: {
					galleryId: galleryId,
					name: newGallery.name,
					children: newGallery.children
				}
			}).success(function(gallery) {
				$scope.gallery = gallery;
				$scope.title = gallery.name;
			}).error(function() {
				alert("Update gallery failed!");
			});
		});
	};

	$scope.openChild = function(child) {
		Gallery.setChild(child);
		$location.path('/child/' + $routeParams.gallery + '/' + child._id);
	};

	var deleteChild = function(obj) {
		var dlg = dialogs.confirm('Confirm delete', 'Are you sure you want to delete this child?', {backdrop: 'static'});
		dlg.result.then(function() {
			$http({
				method: "POST",
				url: "deleteChild",
				data: {
					userId: $rootScope.currentUser._id,
					galleryId: $scope.gallery._id,
					childId: obj.item._id
				}
			}).success(function(data) {
				if (data.deleted >= 0) {
					$scope.gallery.children.splice(obj.index, 1);
				}
			}).error(function() {
				alert("delete child failed!");
			});
		});
	};

	$scope.menuItems = [
		{
			name: "Delete",
			func: deleteChild
		}
	];
}).config(['$compileProvider',
  function($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|whatsapp):/);
  }
]);
