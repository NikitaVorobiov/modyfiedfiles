;(function () {
	'use strict';

	angular.module('createAlbumsApp').controller('GroupTemplatesCtrl', function($scope, $rootScope, $location, $http, dialogs, Gallery, $routeParams) {
		console.log(true);

		var groupId = $routeParams.group;

		$http.get('/getGroups/' + groupId)
			.then(function success (groupInfo) {
				$scope.templates = groupInfo.data[0].templates;
				$scope.openTemplate = function(template){
					console.log(template);
					$location.path('/specific-template/' + template.templateId + '/' + groupInfo.data[0]._id);
				};
			}, function error (err) {
				console.log(err);
			});


	});
})();
