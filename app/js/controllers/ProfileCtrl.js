angular.module('createAlbumsApp').controller('ProfileController', function($scope, $http) {
    $scope.master = {};

    $scope.register = function(user) {
        var responsePromise = $http({
            method: "POST",
            url:"createUser",
            data: user
        });

        responsePromise.success(function(data) {
            if(data.info){
                alert(data.info);
            } else {
                alert("Register succeeded");
            }
        });
        responsePromise.error(function() {
            alert("Register failed!");
        });
    };

    $scope.reset = function() {
        $scope.user = angular.copy($scope.master);
        $scope.user.type = 'designer';
    };

    $scope.reset();
});