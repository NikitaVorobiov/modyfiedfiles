angular.module('createAlbumsApp').controller('AlbumTemplateController', function($scope, General) {
    $scope.host = General.getS3Host();

    // initial image index
    $scope._Index = 0;

    // if a current image is the same as requested image
    $scope.isActive = function(index) {
        return $scope._Index === index;
    };

    // show prev image
    $scope.showPrev = function(callback) {
        if ($scope._Index == 0) {
            return;
        }
        $scope._Index = ($scope._Index > 0)
            ? --$scope._Index
            : $scope.themes.length - 1;
        $scope.currentTheme = $scope.themes[$scope._Index];
        if (callback) {
            callback({theme: $scope.currentTheme});
        }
    };

    // show next image
    $scope.showNext = function(callback) {
        if ($scope._Index == $scope.themes.length - 1) {
            return;
        }
        $scope._Index = ($scope._Index < $scope.themes.length - 1)
            ? ++$scope._Index
            : 0;
        $scope.currentTheme = $scope.themes[$scope._Index];
        if (callback) {
            callback({theme: $scope.currentTheme});
        }
    };

    // show a certain theme
    $scope.jumpToTheme = function(index) {
        $scope._Index = index;
        $scope.currentTheme = $scope.themes[$scope._Index];
    };
});
