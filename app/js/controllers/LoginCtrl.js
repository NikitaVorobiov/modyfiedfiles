angular.module('createAlbumsApp').controller('LoginController', function($scope, $http, $location, Auth, $rootScope) {

	$scope.loginInfo = {};
	$scope.login = function() {
		Auth.login('password', {
				'email': $scope.loginInfo.email,
				'password': $scope.loginInfo.password
			},
			function(err) {
				if (!err) {
					console.log($rootScope.currentUser);
					$http.get('/getUser/' + $rootScope.currentUser._id)
						.then(function success (user) {
							$rootScope.currentUser.sysType = user.data.sysType;
						}, function error (err) {
							console.log(err);
						});
					$location.path('/');
				} else {
					alert(err);
				}
			});
	};

	$scope.logout = function() {
		Auth.logout(function(err) {
			if(!err) {
				$rootScope.currentUser = '';

				$location.path('/login');
			}
		});
	};
});
