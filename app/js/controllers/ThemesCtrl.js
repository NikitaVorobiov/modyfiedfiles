angular.module('createAlbumsApp').controller('ThemesController', function($scope, $rootScope, $http, Templates, $state) {
    $scope.labelInput = {label: "Template name", value: Templates.getTemplateName()};
    $scope.checkedThemes = Templates.getCheckedThemes();
    $scope.displayThemes = [];
    var themes = [];

    $scope.onCheckBoxClick = function(themeObj){
        if(themeObj.checked){
            $scope.checkedThemes.push(themeObj.theme);
        } else {
            $scope.checkedThemes.forEach(function(checkedTheme, index){
                if(checkedTheme._id == themeObj.theme._id){
                    $scope.checkedThemes.splice(index, 1);
                    return false;
                }
            });
        }
    };

    var setScopeThemes = function(){
        var checkedThemes = {};
        angular.forEach(Templates.getCheckedThemes(), function(checkedTheme){
            checkedThemes[checkedTheme._id] = true;
        });
        $scope.displayThemes = themes.map(function(theme){
            return {name:theme.name, checked: checkedThemes[theme._id], theme: theme};
        });
    };

    $scope.next = function() {
        Templates.setCheckedThemes($scope.checkedThemes);
        Templates.setTemplateName($scope.labelInput.value);
        if($scope.checkedThemes.length > 1){
            $state.go('template-theme');
        } else if($scope.checkedThemes.length == 1){
            $state.go('template-theme-last');
        }
    };

    $http.get('getAllUserThemes?userId=' + $rootScope.currentUser._id).success(function(_themes) {
        themes = _themes;
        setScopeThemes();
    });
});

angular.module('createAlbumsApp').controller('ThemeController', function($scope, $rootScope, $routeParams, $http, Upload, Templates, $state, dialogs, General) {
    $scope.theme = Templates.getCurrentTheme();
    $scope.bg = {image:"", coords: []};
    $scope.thumbs = [];
    $scope.coords = [];
    $scope.host = General.getS3Host();
    $scope.mode = "edit";

    var prefix = "users/" + $rootScope.currentUser._id + "/themes/" + $scope.theme._id + "/thumbs/";
    $http.get('/getThumbsList?prefix=' + prefix)
        .success(function(data) {
            $scope.thumbs = data.keys.map(function(keyObj){
                return {
                    src: keyObj.Key
                }
            });
            if($scope.thumbs.length){
                $scope.selectThumb($scope.thumbs[0].src, 0);
            }
        })
        .error(function() {
            alert("Error getting thumbs!");
        });

    $scope.selectThumb = function(fileName, index){
        $scope.bg.image = fileName;
        $scope.bg.index = index;
        $scope.bg.coords = $scope.theme.designs[$scope.bg.index].coords;
        var imageData = {bg:$scope.bg.image, coords:$scope.bg.coords};
        Templates.setThemeImage(imageData, $scope.theme._id);
        $scope.onSelect($scope.bg.coords);
    };

    $scope.next = function() {
        Templates.incrementTheme();
        if(Templates.isLastTheme()){
            $state.go("template-theme-last", {}, {reload:true});
        } else {
            $state.go("template-theme", {}, {reload:true});
        }
    };

    $scope.prev = function() {
        Templates.decrementTheme();
        $state.go("template-theme", {}, {reload:true});
    };

    $scope.isLastTheme = function(){
        return Templates.isLastTheme();
    };

    $scope.isFirstTheme = function(){
        return Templates.isFirstTheme();
    };

    $scope.s3_upload = function(){
        var path = "users/" + $rootScope.currentUser._id + "/themes/" + $scope.theme._id + '/';
        var resizePath = path + 'thumbs/';
        var config = {
            onFinishS3Put: function(public_url, file){
                $http({
                    method: "POST",
                    url:"addThemeDesign",
                    data: {
                        themeId: $scope.theme._id
                    }
                }).success(function(data) {
                    $scope.theme.designs.push(data.design);
                    $scope.selectThumb(newImage.src, $scope.thumbs.length-1);
                }).error(function() {
                    alert("Add theme design failed!");
                });
                var newImage = {src: ""};
                $scope.thumbs.push(newImage);
                if(!$scope.$$phase) {
                    $scope.$apply();
                }
                var srcIndex = $scope.thumbs.length-1;
                Upload.resizeImage({public_url: public_url, resizePath: resizePath, fileType:file.type}, function(key){
                    newImage.src = key;
                    $scope.selectThumb(newImage.src, srcIndex);
                });
                getImageDim(file, function(dim){
                    Upload.splitImage({public_url: public_url, path: path, fileType:file.type, dim: dim, gravity: "west"});
                    Upload.splitImage({public_url: public_url, path: path, fileType:file.type, dim: dim, gravity: "east"});
                });
            },
            createFileNames: function(files){
                Upload.createFileNames($scope.theme._id, files);
            },
            path: path
        };
        dialogs.create('/dialogs/uploadDialog.html', 'uploadDlgController', config, {backdrop:'static'});
    };

    var getImageDim = function(file, callback){
        var image  = new Image();
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function(_file) {
            image.src    = _file.target.result;
            image.onload = function() {
                callback({width: this.width, height: this.height});
            };
            image.onerror= function() {
                alert('Invalid file type: '+ file.type);
            };
        };
    };

    $scope.save = function(){
        $http({
            method: "POST",
            url:"updateThemeDesign",
            data: {
                themeId: $scope.theme._id,
                designId: $scope.theme.designs[$scope.bg.index]._id,
                coords: $scope.coords
            }
        }).success(function(data) {
            if(data.numAffected){
                $scope.theme.designs[$scope.bg.index].coords = $scope.coords;
                $scope.bg.coords = $scope.coords;
                var imageData = {bg:$scope.bg.image, coords:$scope.coords};
                Templates.setThemeImage(imageData, $scope.theme._id);
            }
        }).error(function() {
            alert("Update theme failed!");
        });
    };
});