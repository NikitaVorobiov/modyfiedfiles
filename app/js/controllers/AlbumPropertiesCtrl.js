angular.module('createAlbumsApp').controller('AlbumPropertiesController', function($scope, $rootScope, $location, $http, $routeParams, AlbumView) {
    $scope.labelInput = {label:"Album name", value:""};
    $scope.children = [];
    $scope.selectedChildren = [];
    $scope.translationTexts = {
        buttonDefaultText: ""
    };
    $scope.selectedGallery = {id:-1, name: ""};
    $scope.selectedTemplate = {id:-1, name:""};

    $scope.childrenSettings = {
        enableSearch: true,
        scrollableHeight: '200px',
        scrollable: true,
        idProp: "_id",
        externalIdProp: "",
        displayProp: "name",
        smartButtonMaxItems: 4,
        smartButtonTextConverter: function(itemText) {
            return itemText;
        }
    };

    // $http.get('getGalleries?userId=' + $rootScope.currentUser._id).success(function(galleries) {
    //     if(!galleries.error){
    //         $scope.galleries = galleries;
    //     } else {
    //         alert(galleries.error);
    //     }
    // });
    $scope.galleries = [];
	$http.get('/getUser/' + $rootScope.currentUser._id).then(function success(user) {
		user.data.groups.forEach(function(group) {
			$http.get('/getGroups/' + group.groupId).then(function success(gallery) {
                console.log(gallery);
				$scope.galleries.push(gallery.data[0]);
			}, function error(err) {
				console.log(err);
			})
		})
	}, function error(err) {
		console.error(err);
	});

    // $http.get('getTemplates?userId=' + $rootScope.currentUser._id).success(function(templates) {
    //     if(!templates.error){
    //         $scope.templates = templates;
    //     } else {
    //         alert(templates.error);
    //     }
    // });
    $http.get('getAllTemplates')
        .then(function success (templates) {
            $scope.templates = [];
            for (var i = 0; i < templates.data.length; i++) {
                $scope.templates.push(templates.data[i]);
            }
            console.log($scope.templates);
        }, function error (err) {
            console.log(err);
        })

    $scope.selectGallery = function(gallery){
        $scope.selectedGallery = gallery;
        $http.get('getGalleryContent?galleryId=' + $scope.selectedGallery._id).success(function(gallery) {
            if(!gallery.error){
                $scope.children = gallery.children;
            } else {
                alert(gallery.error);
            }
        });
        $scope.selectedChildren = [];
    };

    $scope.selectTemplate = function(template){
        $scope.selectedTemplate = template;
    };

    $scope.createAlbum = function(){
        AlbumView.setGalleryId($scope.selectedGallery._id);
        AlbumView.setChildId($scope.selectedChildren[0]._id);
        AlbumView.setTemplateId($scope.selectedTemplate._id);
        AlbumView.setAlbumName($scope.labelInput.value);

        var data = {
            userId: $rootScope.currentUser._id,
            galleryId: $scope.selectedGallery._id,
            children: $scope.selectedChildren.map(function(child){
                return {
                    id: child._id,
                    name: child.name
                };
            }),
            templateId: $scope.selectedTemplate._id,
            name: $scope.labelInput.value
        };
        $http({
            method: "POST",
            url:"createAlbums",
            data: data
        }).success(function(albums) {
            if(albums.length == 1){
                AlbumView.setThemes(albums[0].themes);
                $location.path('/albumView/' + albums[0]._id);
            } else {
                $location.path('/albums');
            }
        }).error(function() {
            alert("Create album failed!");
        });
    };
});
