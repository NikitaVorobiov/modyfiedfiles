angular.module('createAlbumsApp').controller('AlbumViewController', function($scope, $rootScope, $location, $http, $routeParams, AlbumView, General, dialogs) {
    var galleryId, childId, templateId;
    var albumId = $routeParams.album;
    $scope.host = General.getS3Host();
    $scope.maxWidth = 100;
    $scope.maxHeight = 100;

    var handleAlbumView = function(){
        $scope.themes = AlbumView.getThemes();
        $scope.currentTheme = $scope.themes[0];
        angular.forEach($scope.themes, function(theme) {
            setBgCoords(theme);
        });
    };

    var getAllThumbsList = function(callback){
        var theme = $scope.currentTheme;
        if(!theme.photos){
            var prefix = "users/" + $rootScope.currentUser._id + "/galleries/" + galleryId + "/" + childId + "/" + theme._id + "/thumbs/";
            $http.get('/getThumbsList?prefix=' + prefix)
                .success(function(data) {
                    console.log(data);
                    theme.photos = data.keys.map(function(keyObj){
                        return {
                            src: keyObj.Key,
                            style: "",
                            filters: {}
                        }
                    });
                    callback();
                })
                .error(function() {
                    alert("Error getting thumbs!");
                });
        } else {
            callback();
        }
    };

    var setBgCoords = function(theme){
        theme.style = [];
        angular.forEach(theme.design.coords, function(coord, index) {
            var photo = theme.displayedPhotos[index];
            if(photo){
                filtersObjToStyle(theme.displayedPhotos[index]);
                var coordsFactor = General.getCoordsFactor();
                theme.style.push({
                    left: coord.x*coordsFactor.xFactor - $scope.maxWidth/2,
                    top: coord.y*coordsFactor.yFactor - $scope.maxHeight/2
                });
            }
        });
    };

    $scope.replacePhotoDlg = function(index){
        getAllThumbsList(function(){
            var dlg = dialogs.create('/dialogs/replacePhotoDialog.html', 'replacePhotoDlgController', {photos: $scope.currentTheme.photos, index: index}, {backdrop:'static'});
            dlg.result.then(replacePhoto);
        });
    };

    var replacePhoto = function(data){
        $scope.currentTheme.displayedPhotos[data.index] = $scope.currentTheme.photos[data.selectedPhoto];
    };

    $scope.editPhotoDlg = function(index){
        var dlg = dialogs.create('/dialogs/editPhotoDialog.html', 'editPhotoDlgController', {photo:$scope.currentTheme.displayedPhotos[index]}, {backdrop:'static'});
        dlg.result.then(function(filters){
            editPhoto(filters, index);
        });
    };

    var filtersObjToStyle = function(photo){
        if(photo.filters){
            if(Object.keys(photo.filters).length){
                photo.style = "-webkit-filter:";
                angular.forEach(photo.filters, function(filter, label){
                    photo.style += " " + label + "(" + filter.value +  filter.unit + ")";
                });
            } else {
            }
        }
        else {
            photo.filters = {};
        }
    };

    var filtersToStyle = function(filters, photo){
        angular.forEach(filters, function(filter){
            var currentPhotoFilter = photo.filters[filter.label];
            if(!currentPhotoFilter || currentPhotoFilter.value != filter.value){
                photo.filters[filter.label] = {value: filter.value, unit: filter.unit};
            }
        });
        filtersObjToStyle(photo);
    };

    var editPhoto = function(filters, index){
        var photo = $scope.currentTheme.displayedPhotos[index];
        filtersToStyle(filters, photo);
    };

    $scope.saveAlbum = function(){
        var themes = $scope.themes.map(function(theme){
            return {
                _id: theme._id,
                displayedPhotos: theme.displayedPhotos.map(function(photo){
                    return {src:photo.src, filters:photo.filters};
                }),
                design:theme.design
            };
        });
        var data = {
            userId: $rootScope.currentUser._id,
            galleryId: galleryId,
            childId: childId,
            templateId: templateId,
            name: AlbumView.getAlbumName(),
            themes: themes,
            _id: albumId
        };
        $http({
            method: "POST",
            url:"saveAlbum",
            data: data
        }).success(function(album, status, headers, config) {
        }).error(function() {
            alert("Save album failed!");
        });
    };

    $scope.testCanvas = function(){
        var $ = jQuery;
        var albumEl = $(".album-view-container");

        angular.forEach(albumEl.find(".theme-album"), function(theme){
            var leftBgImg = angular.element(theme).find(".theme-bg-image").addClass("left")[0];
            var rightBgImg = jQuery("<img>").addClass("right theme-bg-image").prependTo(theme);
            var pathArr = leftBgImg.src.split("/");
            var newPath = "";
            angular.forEach(pathArr, function(pathItem){
                if(pathItem.toLowerCase().indexOf("thumb") > -1){
                    var thumbIndex = pathItem.indexOf("Thumb");
                    if(thumbIndex > -1){
                        newPath += pathItem.substring(0, thumbIndex);
                    } else {
                        newPath += "split/";
                    }
                } else {
                    newPath += pathItem + "/";
                }
            });
            leftBgImg.src = newPath + "Left";
            rightBgImg[0].src =  newPath + "Right";
        });


        albumEl.find(".change-photo, .arrow, .buttons-container, .nav").remove();
        albumEl.find(".album-view").addClass("pdfMode");
        var siteWidth = 435;
        var siteHeight = 300;
        $.each(albumEl.find(".theme-album"), function(index, theme){
            var bgHeight = 1416;
            var bgWidth = 1000;
            var $theme = $(theme);
            var leftBgImg = $theme.find(".theme-bg-image.left");
            var rightBgImg = $theme.find(".theme-bg-image.right");
            var leftBgWrapper = $("<div>").addClass("images-wrapper").prependTo($theme);
            var rightBgWrapper = $("<div>").addClass("images-wrapper").prependTo($theme);
            leftBgImg.appendTo(leftBgWrapper);
            rightBgImg.appendTo(rightBgWrapper);
            leftBgImg.height(bgHeight);
            leftBgImg.width(bgWidth);
            rightBgImg.height(bgHeight);
            rightBgImg.width(bgWidth);
            //draw images
            var xFactor = (bgWidth*2)/siteWidth;
            var yFactor = bgHeight/siteHeight;
            var images = $theme.find(".image-wrapper");
            $.each(images ,function(index, imageWrapper){
                var $imageWrapper = $(imageWrapper);
                var image = $imageWrapper.find(".image");
                var left = parseInt($imageWrapper.css("left"));
                var top = parseInt($imageWrapper.css("top"));
                $imageWrapper.width(100*xFactor);
                $imageWrapper.height(100*yFactor);
                image.css("maxWidth", 100*xFactor + "px");
                image.css("maxHeight", 100*yFactor + "px");
                $imageWrapper.css("top", parseInt($imageWrapper.css("top"))*yFactor);
                if(left < siteWidth/2){
                    leftBgWrapper.append($imageWrapper);
                    $imageWrapper.css("left", parseInt($imageWrapper.css("left"))*xFactor);
                } else {
                    rightBgWrapper.append($imageWrapper);
                    $imageWrapper.css("left", (parseInt($imageWrapper.css("left")) - siteWidth/2)*xFactor);
                }
            });
        });
    };

    var parseToHiRes = function(albumEl){
        angular.forEach(albumEl.find(".theme-album"), function(theme){
            var leftBgImg = angular.element(theme).find(".theme-bg-image").addClass("left")[0];
            var rightBgImg = jQuery("<img>").addClass("right theme-bg-image").prependTo(theme);
            var pathArr = leftBgImg.src.split("/");
            var newPath = "";
            angular.forEach(pathArr, function(pathItem){
                if(pathItem.toLowerCase().indexOf("thumb") > -1){
                    var thumbIndex = pathItem.indexOf("Thumb");
                    if(thumbIndex > -1){
                        newPath += pathItem.substring(0, thumbIndex);
                    } else {
                        newPath += "split/";
                    }
                } else {
                    newPath += pathItem + "/";
                }
            });
            leftBgImg.src = newPath + "Left";
            rightBgImg[0].src =  newPath + "Right";
        });
        return albumEl[0].outerHTML;
    };

    $scope.createPdf = function(){
        General.toggleSpinner();
        $http({
            method: "POST",
            url:"createPdf",
            data: {
                url: parseToHiRes(angular.element(".album-view-container").clone()),
                s3_object_name:"pdf_" + albumId,
                s3_object_type: 'application/pdf',
                key: 'users/' + $rootScope.currentUser._id + '/pdf/'

            }
        }).success(function() {
            General.toggleSpinner();
            alert("Created pdf!");
        }).error(function() {
            General.toggleSpinner();
            alert("Create pdf failed!");
        });
    };

    $scope.albumLink = function(){
        var protocol = $location.protocol();
        console.log(protocol);
        var host = $location.host();
        var port = $location.port() == "5000" ? ":" + $location.port() : "";
        var link = protocol + "://" + host + port + "/albumViewReadOnly/" + albumId;
        dialogs.create('/dialogs/albumLinkDialog.html', 'albumLinkDlgController', {link: link}, {backdrop:'static'});
    };
    console.log($rootScope.currentUser)
    $scope.changeThemeCallback = function(theme){
        $scope.currentTheme = theme;
    };

    galleryId = AlbumView.getGalleryId();
    childId = AlbumView.getChildId();
    templateId = AlbumView.getTemplateId();
    if(albumId && !galleryId){
        $http.get('/getAlbumContent?albumId=' + albumId)
            .success(function(data) {
                if(data){
                    galleryId = data.galleryId;
                    childId = data.childId;
                    templateId = data.templateId;
                    AlbumView.setAlbumName(data.name);
                    AlbumView.setThemes(data.themes);
                    handleAlbumView();
                } else{
                    $location.path("/albums");
                }

            })
            .error(function() {
                alert("Error getting album!");
                $location.path("/albums");
            });
    } else if(!galleryId){
        $location.path("/albums");
    } else {
        handleAlbumView();
    }
});
