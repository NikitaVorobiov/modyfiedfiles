(function() {

	'use strict';

	angular.module('createAlbumsApp').controller('SpecificTemplateCtrl', function($scope, $rootScope, $http, $location, $routeParams, General, Templates, dialogs, Upload, Gallery, $q) {

		var templateId = $routeParams.template;
		var groupId = $routeParams.group;

		$scope.host = General.getS3Host();
		$scope.childInfo = {};

		$scope.groupTemplateLink = $location.absUrl();
		$scope.emailForm = false;

		$scope.sendTemplateLink = function sendTemplateLink () {
			if ($scope.sendInfo.recipient) {
				$http.post('/sendEmail', {
					recipient: $scope.sendInfo.recipient,
					html: 'Template Link : ' + $scope.groupTemplateLink,
					subject: 'Template'
				}).then(function success (res) {
					$scope.emailForm = false;
				}, function error (err) {
					console.log(err);
				});
			} else {
				alert('Please, write an email !!!')
			}
		}

		// Template part
		$http.get('/getTemplateContent/' + templateId)
			.then(function success (tpl) {
				$scope.template = tpl.data;
				drawCoords();
			}, function error (err) {
				console.log(err);
			});

		// Initial image index
		$scope._Index = 0;

		var drawCoords = function(){
			$scope.currentTheme = $scope.template.themes[$scope._Index];
			var coords = $scope.currentTheme.design.coords.map(function(coord){
				var coordsFactor = General.getCoordsFactor();
				return {x:coord.x*coordsFactor.xFactor, y:coord.y*coordsFactor.yFactor};
			});
			$scope.onSelect(coords);
		};

		// if a current image is the same as requested image
		$scope.isActive = function (index) {
			return $scope._Index === index;
		};

		// show prev image
		$scope.showPrev = function () {
			if($scope._Index == 0){
				return;
			}
			$scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.template.themes.length - 1;
			$scope.currentTheme = $scope.template.themes[$scope._Index];
			drawCoords();
		};

		// show next image
		$scope.showNext = function () {
			if($scope._Index == $scope.template.themes.length - 1){
				return;
			}
			$scope._Index = ($scope._Index < $scope.template.themes.length - 1) ? ++$scope._Index : 0;
			$scope.currentTheme = $scope.template.themes[$scope._Index];
			drawCoords();
		};

		// show a certain theme
		$scope.jumpToTheme = function (index) {
			$scope._Index = index;
			$scope.currentTheme = $scope.template.themes[$scope._Index];
			drawCoords();
		};

		$http.get('/getGroups/' + groupId)
			.then(function success (group) {
				$scope.group = group.data[0];
				$scope.children = group.data[0].children;
			}, function error (err) {
				console.log(err);
			});

		$scope.showUploadBtn = true;
		$scope.childId = '';
		$scope.newChildInfo = {};
		$scope.newChildInfo.photos = [];
		$scope.createChildForm = false;
		$scope.showChildBtns = false;

		$scope.createNewChild = function createNewChild () {
			$http.post('/updateGroupChildren', {
			    "groupId": groupId,
			    "childInfo": $scope.newChildInfo
			}).then(function success (result) {
				$scope.newChildren = result.data.children;
				for (var i = 0; i < $scope.newChildren.length; i++) {
					if ($scope.newChildren[i].name === $scope.newChildInfo.name) {
						$scope.newChildInfo._id = $scope.newChildren[i]._id;
					}
				};
				$scope.children.push($scope.newChildInfo);
				$scope.newChildInfo = {};
				$scope.createChildForm = false;
			}, function error (err) {
				console.log(err);
			})
		};

		$scope.createAutoAlbum = function createAutoAlbum () {
			if ($scope.childId) {
				alert($scope.childId);
			} else {
				alert('Please, select your child');
			}
		};

		$scope.viewThemesSelect = false;

		$scope.viewThemes = function viewThemes () {
			$scope.viewThemesSelect = true;
			var getChildThumbs = function(){
				var prefix = "users/" + $rootScope.currentUser._id + "/galleries/" + groupId + "/" + $scope.childId + "/";
				var def = $q.defer();
				$http.get('/getThumbsListInGallery?prefix=' + prefix)
				.success(function(thumbsObj) {
					def.resolve(thumbsObj);
				})
				.error(function() {
					alert("Error getting thumbs!");
				});
				return def.promise;
			};

			var getAllThemesNames = function(){
				var def = $q.defer();
				$http.get('getAllThemesNames?userId=' + $rootScope.currentUser._id).success(function(themes) {
					def.resolve(themes);
				});
				return def.promise;
			};
			$scope.themes = [];

			if($scope.childId && groupId){
				if(Gallery.getChild()){
					$scope.child = Gallery.getChild();
					$scope.title = $scope.child.name + " photos";
				} else {
					$http.get('getChildInGallery?userId=' + $rootScope.currentUser._id + '&childId=' + $scope.childId + '&galleryId=' + groupId).success(function(child) {
						$scope.child = child;
						$scope.title = $scope.child.name + " photos";
					});
				}
				var currentThemesMap = {};
				$q.all([
					getChildThumbs(),
					getAllThemesNames()
				]).then(function (values) {
					console.log(values);
					$scope.allThemes = values[1];
					var allThemesMap = {};
					$scope.allThemes.forEach(function(theme){
						allThemesMap[theme._id] = theme;
					});
					// var themeId = $scope.tplTheme._id;
					console.log(allThemesMap);
					angular.forEach(values[0], function(themeThumbs, themeId){
						var theme = {_id: themeId, name: allThemesMap[themeId].name, thumbs: themeThumbs};
						currentThemesMap[themeId] = theme;
						$scope.themes.push(theme);
					});
				});
			} else {
				console.log(false);
			}
			$scope.tplTheme = {};

			$scope.s3_upload = function s3_upload () {
				var theme = JSON.parse($scope.tplTheme);
				var path = 'users/' + $rootScope.currentUser._id + '/galleries/' + groupId + '/' + $scope.childId + '/' + theme._id + '/';
				var resizePath = path + 'thumbs/';
				var themeId = theme._id;
				var themeName = theme.name;
				var config = {
					onFinishS3Put: function(public_url, file){
						var newImage = {src: ""};
						console.log(currentThemesMap);
						var theme = currentThemesMap[themeId];
						if(theme){
							theme.thumbs.push(newImage);
						} else {
							theme = {_id: themeId,name: themeName, thumbs:[newImage]};
							currentThemesMap[themeId] = theme;
							$scope.themes.push(theme);
						}
						if(!$scope.$$phase) {
							$scope.$apply();
						}
						Upload.resizeImage({public_url: public_url, resizePath: resizePath, fileType:file.type}, function(key){
							newImage.src = key;
						});
					},
					createFileNames: function(files){
						Upload.createFileNames(theme._id, files);
					},
					path: path
				};
				dialogs.create('/dialogs/uploadDialog.html', 'uploadDlgController', config, {backdrop:'static'});
			};
		};



	}).config([
		'$compileProvider',
		function($compileProvider) {
			$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|whatsapp):/);
		}
	]);

})()
