angular.module('createAlbumsApp').controller('ChildController', function($scope, $rootScope, $location, $http, $routeParams, Gallery, dialogs, Upload, General, $q) {
    var galleryId = $routeParams.gallery;
    console.log($routeParams);
    var childId = $routeParams.child;
    $scope.themes = [];
    $scope.allThemes = [];
    var currentThemesMap = {};

    $scope.rights = {
        teacher: false,
        proffesional: false
    };

if ($rootScope.currentUser.type === 'teacher') {
    $scope.rights.teacher = true;
} else if ($rootScope.currentUser.type === 'proffesional') {
    $scope.rights.proffesional = true;
}

var getChildThumbs = function() {
    var prefix = "users/" + $rootScope.currentUser._id + "/galleries/" + galleryId + "/" + childId + "/";
    var def = $q.defer();
    $http.get('/getThumbsListInGallery?prefix=' + prefix).success(function(thumbsObj) {
        console.log(thumbsObj);
        def.resolve(thumbsObj);
    }).error(function() {
        alert("Error getting thumbs!");
    });
    return def.promise;
};

var getAllThemesNames = function() {
    var def = $q.defer();
    $http.get('getAllThemesNames?userId=' + $rootScope.currentUser._id).success(function(themes) {
        def.resolve(themes);
    });
    return def.promise;
};

if (childId && galleryId) {
    if (Gallery.getChild()) {
        $scope.child = Gallery.getChild();
        $scope.title = $scope.child.name + " photos";
    } else {
        $http.get('getChildInGallery?userId=' + $rootScope.currentUser._id + '&childId=' + childId + '&galleryId=' + galleryId).success(function(child) {
            $scope.child = child;
            $scope.title = $scope.child.name + " photos";
        });
    }

    $q.all([getChildThumbs(), getAllThemesNames()]).then(function(values) {
        $scope.allThemes = values[1];
        var allThemesMap = {};
        $scope.allThemes.forEach(function(theme) {
            allThemesMap[theme._id] = theme;
        });
        angular.forEach(values[0], function(themeThumbs, themeId) {
            console.log(allThemesMap[themeId]);
            console.log(themeId);
            var theme = {
                _id: themeId,
                name: allThemesMap[themeId].name,
                thumbs: themeThumbs
            };
            currentThemesMap[themeId] = theme;
            $scope.themes.push(theme);
        });
        console.log(allThemesMap);
    });
} else {
    $location.path('/galleries');
}

$scope.s3_upload = function(theme) {
    console.log(theme);
    var path = 'users/' + $rootScope.currentUser._id + '/galleries/' + galleryId + '/' + childId + '/' + theme._id + '/';
    var resizePath = path + 'thumbs/';
    var themeId = theme._id;
    var themeName = theme.name;
    var config = {
        onFinishS3Put: function(public_url, file) {
            var newImage = {
                src: ""
            };
            var theme = currentThemesMap[themeId];
            if (theme) {
                theme.thumbs.push(newImage);
            } else {
                theme = {
                    _id: themeId,
                    name: themeName,
                    thumbs: [newImage]
                };
                currentThemesMap[themeId] = theme;
                $scope.themes.push(theme);
            }
            if (!$scope.$$phase) {
                $scope.$apply();
            }
            Upload.resizeImage({
                public_url: public_url,
                resizePath: resizePath,
                fileType: file.type
            }, function(key) {
                newImage.src = key;
            });
        },
        createFileNames: function(files) {
            Upload.createFileNames(theme._id, files);
        },
        path: path
    };
    dialogs.create('/dialogs/uploadDialog.html', 'uploadDlgController', config, {backdrop: 'static'});
};

var deletePhoto = function(obj) {
    var dlg = dialogs.confirm('Confirm delete', 'Are you sure you want to delete this photo?', {backdrop: 'static'});
    dlg.result.then(function() {
        var thumb = obj.item;
        $http({
            method: "POST",
            url: "deletePhoto",
            data: {
                thumb: thumb.src
            }
        }).success(function(data) {
            if (data.deleted) {
                currentThemesMap[obj.id].thumbs.splice(obj.index, 1);
            }
        }).error(function() {
            alert("delete photo failed!");
        });
    });
};

var rotateLeft = function(obj) {
    rotate(obj, "rotateLeft");
};

var rotateRight = function(obj) {
    rotate(obj, "rotateRight");
};

var rotate = function(obj, rotateType) {
    var host = General.getS3Host();
    var rotatePath = obj.item.src.substring(0, obj.item.src.lastIndexOf("/") + 1);
    var hiResRotatePath = obj.item.src.substring(0, obj.item.src.lastIndexOf("thumbs/"));
    var questionMarkIndex = obj.item.src.lastIndexOf("?");
    var src = questionMarkIndex != -1
        ? obj.item.src.substring(0, obj.item.src.lastIndexOf("?"))
        : obj.item.src;
    var fileName = src.substring(src.lastIndexOf("thumbs/") + 7, src.lastIndexOf("Thumb"));
    General.toggleSpinner();
    //rotate thumb
    Upload.rotateImage({
        public_url: host + src,
        rotatePath: rotatePath,
        fileType: "image/png",
        rotateType: rotateType
    }, function(key) {
        obj.item.src = key + "?" + new Date().getTime();
        General.toggleSpinner();
    }, function() {
        General.toggleSpinner();
    });
    //rotate hi-res
    Upload.rotateImage({
        public_url: host + hiResRotatePath + fileName,
        rotatePath: hiResRotatePath,
        fileType: "image/png",
        rotateType: rotateType
    }, function(key) {}, function() {});
};

$scope.menuItems = [
    {
        name: "Delete",
        func: deletePhoto
    }, {
        name: "Rotate left",
        func: rotateLeft
    }, {
        name: "Rotate right",
        func: rotateRight
    }
];
});
