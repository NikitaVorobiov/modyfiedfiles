angular.module('createAlbumsApp').controller('HomeController', function($scope, $interval, $rootScope) {
    $scope.slides = [
        {image: '../../images/slider1.jpg', description: 'Image 01'},
        {image: '../../images/slider2.jpg', description: 'Image 02'}
    ];

    

    $scope.currentIndex = 0;

    $scope.setCurrentSlideIndex = function (index) {
        $scope.currentIndex = index;
        $scope.startSwitchPhoto();
    };

    $scope.isCurrentSlideIndex = function (index) {
        return $scope.currentIndex === index;
    };

    $scope.prevSlide = function () {
        $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
    };

    $scope.nextSlide = function () {
        $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
    };

    var switchPhoto;
    $scope.stopSwitchPhoto = function() {
        $interval.cancel(switchPhoto);
    };

    $scope.startSwitchPhoto = function() {
        $scope.stopSwitchPhoto();
        switchPhoto = $interval(function() {
            $scope.nextSlide();
        }, 4000);
    };
    $scope.startSwitchPhoto();

    $scope.$on('$destroy', function() {
        // Make sure that the interval is destroyed too
        $scope.stopSwitchPhoto();
    });
})
    .animation('.slide-animation', function () {
        return {
            addClass: function (element, className, done) {
                if (className == 'ng-hide') {
                    TweenMax.to(element, 0.5, {left: -element.parent().width(), onComplete: done });
                }
                else {
                    done();
                }
            },
            removeClass: function (element, className, done) {
                if (className == 'ng-hide') {
                    element.removeClass('ng-hide');

                    TweenMax.set(element, { left: element.parent().width() });
                    TweenMax.to(element, 0.5, {left: 0, onComplete: done });
                }
                else {
                    done();
                }
            }
        };
    });
