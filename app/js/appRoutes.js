angular.module('createAlbumsApp').config(['$routeProvider', '$locationProvider', '$stateProvider', function($routeProvider, $locationProvider, $stateProvider) {

    var originalWhen = $routeProvider.when;

    $routeProvider.when = function(path, route){
        route.resolve = route.resolve || {};
        angular.extend(route.resolve, {
            currentUser: function(Auth) {
                return Auth.currentUser();
            }
        });
        return originalWhen.call($routeProvider, path, route);
    };

    $routeProvider
        .when('/', {
            templateUrl: 'partials/home.html',
            controller: 'HomeController'
        })
        .when('/register', {
            templateUrl: 'partials/register.html',
            controller: 'RegisterController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"register", url:"register.css"},
                        {id:"labelInput", url:"labelInput.css"}
                    ]);
                }]
            }
        })
        .when('/login', {
            templateUrl: 'partials/login.html',
            controller: 'LoginController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"login", url:"login.css"},
                        {id:"labelInput", url:"labelInput.css"}
                    ]);
                }]
            }
        })
        .when('/profile', {
            templateUrl: 'partials/profile.html',
            controller: 'LoginController'
        })
        .when('/galleries', {
            templateUrl: 'partials/galleries.html',
            controller: 'GalleriesController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"galleries", url:"galleries.css"},
                        {id:"thumbsList", url:"thumbsList.css"}
                    ]);
                }]
            }
        })
        .when('/galleries/:gallery', {
            templateUrl: 'partials/gallery.html',
            controller: 'GalleryController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"galleries", url:"galleries.css"},
                        {id:"thumbsList", url:"thumbsList.css"}
                    ]);
                }]
            }
        })
        .when('/groupTemplates/:group', {
            templateUrl: 'partials/groupTemplates.html',
            controller:  'GroupTemplatesCtrl'
        })
        .when('/joinGroup/:gallery', {
            templateUrl: 'partials/join.html',
            controller: 'JoinGroupCtrl',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set ([
                        {id:"join", url:"join.css"}
                    ])
                }]
            }
        })
        .when('/createTemplate', {
          templateUrl: 'partials/createTemplate.html',
          controller: 'createTemplateCtrl'
        })
        .when('/templates/:template', {
            templateUrl: 'partials/template.html',
            controller: 'TemplateController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"albumView", url:"albumView.css"},
                        {id:"thumbsList", url:"thumbsList.css"},
                        {id:"template", url:"template.css"}
                    ]);
                }]
            }
        })
        .when('/specific-template/:template/:group', {
            templateUrl: 'partials/specific-template.html',
            controller: 'SpecificTemplateCtrl',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id: "albumView", url: "albumView.css"},
                        {id: "thumbsList", url: "thumbsList.css"},
                        {id: "template", url: "template.css"}
                    ]);
                }]
            }
        })
        .when('/templates', {
            templateUrl: 'partials/templates.html',
            controller: 'TemplatesController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"thumbsList", url:"thumbsList.css"}
                    ]);
                }]
            }
        })
        .when('/albums', {
            templateUrl: 'partials/albums.html',
            controller: 'AlbumsController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"thumbsList", url:"thumbsList.css"}
                    ]);
                }]
            }
        })
        .when('/child/:gallery/:child', {
            templateUrl: 'partials/child.html',
            controller: 'ChildController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"thumbsList", url:"thumbsList.css"},
                        {id:"child", url:"child.css"}
                    ]);
                }]
            }
        })
        .when('/albumProperties', {
            templateUrl: 'partials/albumProperties.html',
            controller: 'AlbumPropertiesController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"albumProperties", url:"albumProperties.css"},
                        {id:"labelInput", url:"labelInput.css"}
                    ]);
                }]
            }
        })
        .when('/albumViewReadOnly/:album', {
            templateUrl: 'partials/albumViewReadOnly.html',
            controller: 'AlbumViewController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"albumView", url:"albumView.css"},
                        {id:"cssEditor", url:"cssEditor.css"},
                        {id:"thumbsList", url:"thumbsList.css"}
                    ]);
                }]
            }
        })
        .when('/tpl/:ide', {
          template: 'readOnly'
        })
        .when('/albumView/:album', {
            templateUrl: 'partials/albumView.html',
            controller: 'AlbumViewController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"albumView", url:"albumView.css"},
                        {id:"cssEditor", url:"cssEditor.css"},
                        {id:"thumbsList", url:"thumbsList.css"}
                    ]);
                }]
            }
        })
        .otherwise({
            redirectTo: '/'
        });

    $stateProvider
        .state('default-state',{template:''})
        .state('create-template', {
            templateUrl: 'partials/themes.html',
            controller: 'ThemesController',
            resolve: {
                load: ['injectCSS', function (injectCSS) {
                    return injectCSS.set([
                        {id:"theme", url:"theme.css"},
                        {id:"labelInput", url:"labelInput.css"}
                    ]);
                }]
            }
        })
        .state('template-theme', {
            templateUrl: 'partials/theme.html',
            controller: 'ThemeController'
        })
        .state('template-theme-last', {
            templateUrl: 'partials/theme.html',
            controller: 'ThemeController'
        });

    $locationProvider.html5Mode(true);
}]);
