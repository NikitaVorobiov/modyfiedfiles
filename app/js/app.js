angular.module('createAlbumsApp', [
    'ngCookies',
    'ngResource',
//    'ngSanitize',
    'ngRoute',
    'ngAnimate', 'ngTouch',
    'gatedScope',
//    'http-auth-interceptor',
    'ui.bootstrap','dialogs.main','pascalprecht.translate','dialogs.default-translations',
    'ui.router',
    'angularFileUpload',
    'angularSpinner',
    'angularjs-dropdown-multiselect'
])
    .run(function ($rootScope, $location, Auth) {

        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            var path = $location.path();
            if ((['/', '/login', '/logout', '/register'].indexOf(path) == -1 ) && path.indexOf('/albumViewReadOnly') != 0) {
                if(current){
                    Auth.isCurrentUser();
                } else {
                    Auth.currentUser().then(function (hasSession) {
                        if (!hasSession) {
                            $location.path("/login");
                        }
                    });
                }
            }
        });

        // On catching 401 errors, redirect to the login page.
        $rootScope.$on('event:auth-loginRequired', function() {
            $location.path('/login');
            return false;
        });
    });