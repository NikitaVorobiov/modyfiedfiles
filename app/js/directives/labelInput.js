angular.module('createAlbumsApp')
    .directive("labelInputTable", function(){
        return {
            restrict: "E",
            link: function(scope, element){
                element.addClass('labelInput-elements');
            },
            template:
                '<label-input ng-repeat="labelInput in labelInputs" labelinput="labelInput"></label-input>'
        }}).directive("labelInput", function(){
        return {
            restrict: "E",
            link: function(scope, element){
                element.addClass('labelInput-element');
            },
            scope:{
                labelinput: '='
            },
            template:
                '<label class="labelInput-label">{{labelinput.label}}: </label>' +
                '<input ng-model="labelinput.value" class="labelInput-input">'
        }});
