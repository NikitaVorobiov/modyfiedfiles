angular.module('createAlbumsApp')
    .directive("cssEditor", function(){
        return {
            restrict: "E",
            template:
                '<div class="css-editor" ng-controller="CssEditorController">'+
                    '<img class="thumb" ng-src="{{photo.src ? host + photo.src : \'\'}}" style="-webkit-filter:' +
                        'grayscale({{filters[0].value}}) ' +
                        'sepia({{filters[1].value}}) ' +
                        'saturate({{filters[2].value}}) ' +
                        'hue-rotate({{filters[3].value}}deg) ' +
                        'invert({{filters[4].value}}) ' +
                        'opacity({{filters[5].value}}) ' +
                        'brightness({{filters[6].value}}) ' +
                        'contrast({{filters[7].value}}) ' +
                        'blur({{filters[8].value}}px);' +
                    '">'+
                    '<div class="filter-elements-wrapper">'+
                        '<div class="filter-elements">'+
                            '<css-editor-element class="filter-wrapper" ng-repeat="filter in filters" filter="filter"></css-editor-element>'+
                        '</div>'+
                        '<button ng-click="reset()">Reset to defaults</button>'+
                    '</div>'+
                '</div>'
        }})
    .directive("cssEditorElement", function(){
        return {
            restrict: "E",
            scope:{
                filter: '='
            },
            template:
                '<div class="filter-element">' +
                    '<label class="filter-label">{{filter.label}}</label>' +
                    '<input ng-model="filter.value" type="range" data-default={{filter.default}} value={{filter.value}} min={{filter.min}} max={{filter.max}} step={{filter.step}}>' +
                    '<p class="filter-value">{{filter.value}}</p>' +
                '</div>'
        }})
    .controller('CssEditorController', function($scope, $timeout) {
        $scope.filters = [
            {label:"Grayscale", value:0.0, default:0, min:0, max:1, step:0.1, unit:""},
            {label:"Sepia", value:0, default:0, min:0, max:1, step:0.1, unit:""},
            {label:"Saturate", value:1, default:1, min:0, max:1, step:0.1, unit:""},
            {label:"Hue-rotate", value:0, default:0, min:0, max:360, step:1, unit:"deg"},
            {label:"Invert", value:0, default:0, min:0, max:1, step:0.1, unit:""},
            {label:"Opacity", value:1, default:1, min:0, max:1, step:0.1, unit:""},
            {label:"Brightness", value:1, default:1, min:0, max:1, step:0.1, unit:""},
            {label:"Contrast", value:1, default:1, min:0, max:2, step:0.1, unit:""},
            {label:"Blur", value:0, default:0, min:0, max:10, step:1, unit:"px"}
        ];

        var previousFilters = $scope.$parent.photo.filters;
        if(Object.keys(previousFilters).length){
            angular.forEach($scope.filters, function(filter){
                if(previousFilters[filter.label]){
                    filter.value = previousFilters[filter.label].value;
                }
            });
        }

        $timeout(function(){
            var inputElements = angular.element(".filter-element input[type='range']");
            angular.forEach(inputElements, function(inputElement, index){
                inputElement.value = $scope.filters[index].value;
            });
        }, 0);

        $scope.reset = function(){
            angular.forEach($scope.filters, function(filter){
                filter.value = filter.default;
            });
        };

        $scope.$parent.getFilters = function(){
            return $scope.filters;
        }
    });