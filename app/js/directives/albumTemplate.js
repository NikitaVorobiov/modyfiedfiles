angular.module('createAlbumsApp')
    .directive("albumTemplate", function(){
        return {
            restrict: "E",
            controller: "AlbumTemplateController",
            scope:{
                themes: '=',
                // readOnly: '=',
                replacePhotoDlg: '&',
                editPhotoDlg: '&',
                changeThemeCallback: '&'
            },
            template:
                '<div class="album-view">'+
                    '<div class="theme-album slide" ng-repeat="theme in themes track by $index" ng-swipe-right="showPrev(changeThemeCallback)" ng-swipe-left="showNext(changeThemeCallback)" ng-show="isActive($index)">'+
                        '<img class="theme-bg-image" ng-src="{{theme.design.bg ? host + theme.design.bg : \'\'}}">'+
                            '<div ng-mouseenter="showReplace = true" ng-mouseleave="showReplace = false"'+
                                'sly-repeat="photo in theme.displayedPhotos | limitTo:theme.style.length" class="image-wrapper"'+
                                        'ng-style="{'+
                                'left: theme.style[$index].left,'+
                                'top: theme.style[$index].top'+
                            '}">'+
                                '<div class="image-container" ng-click="editPhotoDlg({index:$index})">'+
                                    '<img class="image fit-self" ng-class="{blank: !photo.src}" ng-src="{{photo.src ? host + photo.src : \'\'}}" style="{{photo.style}}"/>'+
                                    '<div ng-show="showReplace " class="change-photo fa fa-pencil-square-o" title="Replace photo"'+
                                        'ng-click="replacePhotoDlg({index:$index}); $event.stopPropagation();"></div>'+
                                '</div>'+
                            '</div>'+
                    '</div>'+
                    '<a class="fa fa-arrow-left arrow prev" href="#" ng-click="showPrev(changeThemeCallback)" ng-disabled="!_Index"></a>'+
                    '<a class="fa fa-arrow-right arrow next" href="#" ng-click="showNext(changeThemeCallback)" ng-disabled="_Index == themes.length - 1"></a>'+
                '</div>'+
                //<!--extra navigation controls -->
                '<ul class="nav">'+
                    '<li ng-repeat="theme in themes" ng-class="{\'active\':isActive($index)}">'+
                        '<img ng-src="{{theme.design.bg ? host + theme.design.bg : \'\'}}" title="{{theme.name}}" ng-click="jumpToTheme($index);" />'+
                    '</li>'+
                '</ul>'
        }});
