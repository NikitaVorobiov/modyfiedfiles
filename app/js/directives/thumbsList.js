angular.module('createAlbumsApp')
    .directive("thumbsList", function(){
        return {
            restrict: "E",
            controller: "ThumbsListController",
            scope:{
                items: '=',
                id: '=',
                menuItems: '=',
                onClick: '&',
                selectItem: '&',
                active: '=',
                withText: '=',
                thumbsTitle: '='
            },
            template:
                '<div class="thumbs-list-wrapper">{{thumbsTitle}}'+
                    '<div class="filter-wrapper">'+
                        '<input ng-show=withText ng-model="searchString" placeholder="Find">'+
                    '</div>'+
                    '<ul class="thumbs-list">'+
                        '<li class="thumb-item" ng-repeat="item in items | searchFor:searchString track by $index" ng-click="$event.originalEvent.dropdown || onClick({item:item, index:$index})" title="{{item.name}}" ng-class="{active: $index === active, \'with-text\': withText}" ng-mouseenter="showEdit = true" ng-mouseleave="showEdit = false">'+
                            '<img class="thumb" ng-src="{{item.src ? host + item.src : \'\'}}">'+
                            '<div ng-show=withText class="ellipsis">{{item.name}}</div>'+
                            '<div ng-show="showEdit && menuItems" class="btn-group edit-item" ng-click="$event.originalEvent.dropdown = true" dropdown>'+
                                '<button type="button" title="Edit" class="btn btn-default dropdown-toggle fa fa-edit" dropdown-toggle></button>'+
                                '<ul class="dropdown-menu" role="menu">'+
                                    '<li class="entry ellipsis" title="{{menuItem.name}}" ng-repeat="menuItem in menuItems" ng-click="menuItem.func({item:item, index:$parent.$index, id:id})">{{menuItem.name}}</li>'+
                                '</ul>'+
                            '</div>'+
                        '</li>'+
                    '</ul>'+
                '</div>'
        }})
    .filter('searchFor', function(){
        return function(arr, searchString){
            if(!searchString){
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function(item){
                if(item.name.toLowerCase().indexOf(searchString) !== -1){
                    result.push(item);
                }
            });
            return result;
        };
    });
