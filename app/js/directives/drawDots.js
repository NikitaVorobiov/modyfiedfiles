angular.module('createAlbumsApp').directive("drawDots", function(){
    var canvasWidth = 435;
    var canvasHeight = 300;
    return {
        restrict: "A",
        scope:{
            mode: '=',
            coords: '=',
            onSelect: '=',
            reset: '='
        },
        link: function(scope, element){
            var ctx = element[0].getContext('2d');

            element.bind('mousedown', function(event){
                // begins new dot
                if(scope.mode == "edit"){
                    draw(event.offsetX, event.offsetY);
                }
            });

            scope.onSelect = function(bgCoords){
                reset();
                angular.forEach(bgCoords, function(coords){
                    draw(coords.x, coords.y);
                });
            };

            scope.reset = function(){
                reset();
            };

            // canvas reset
            function reset(){
                scope.coords = [];
                ctx.clearRect(0, 0, canvasWidth, canvasHeight);
            }

            function draw(startX, startY){
                if(acceptDraw(startX, startY)){
                    scope.coords.push({x:startX, y:startY});
                    ctx.beginPath();
                    ctx.arc(startX, startY, 5, 0, 2*Math.PI, false);
                    ctx.lineWidth = 3;
                    // color
                    ctx.strokeStyle = 'black';
                    // draw it
                    ctx.stroke();
                }
            }

            function acceptDraw(startX, startY){
                return !(startX < 50 || startX > canvasWidth - 50 ||
                    startY < 50 || startY > canvasHeight - 50
                    || (startX > canvasWidth/2 - 50 && startX < canvasWidth/2 + 50));
            }
        }
    };
});