//Directive that returns an element which adds buttons on click which show an alert on click
angular.module('createAlbumsApp').directive("addnamesbutton", function(){
    return {
        restrict: "E",
        template: "<button addelements>Click to add names</button>"
    }
});

//Directive for adding buttons on click that show an alert on click
angular.module('createAlbumsApp').directive("addelements", function($compile){
    return function(scope, element){
        element.bind("click", function(){
            scope.newGallery.children.push({name: "", mother:{name:"", email:"", phone:""}, father:{name:"", email:"", phone:""}, photos:[]});
            var index = scope.newGallery.children.length - 1;
            angular.element(document.getElementById('space-for-names')).append($compile("<div class='add-child-container'>" +
                "<div class='label-input name'>" +
                    "<label>Name: </label>" +
                    "<input ng-model=newGallery.children["+index+"].name ng-model-options='{ updateOn: blur}'>" +
                "</div>" +
                "<div class='parent'>" +
                    "<div class='label-input'>" +
                        "<label>Mother's name: </label>" +
                        "<input ng-model=newGallery.children["+index+"].mother.name ng-model-options='{ updateOn: blur}'>" +
                    "</div>" +
                    "<div class='label-input'>" +
                        "<label>Mother's phone: </label>" +
                        "<input ng-model=newGallery.children["+index+"].mother.phone ng-model-options='{ updateOn: blur}'>" +
                    "</div>" +
                    "<div class='label-input'>" +
                        "<label>Mother's email: </label>" +
                        "<input ng-model=newGallery.children["+index+"].mother.email ng-model-options='{ updateOn: blur}'>" +
                    "</div>" +
                "</div>" +
                "<div class='parent'>" +
                    "<div class='label-input'>" +
                        "<label>Father's name: </label>" +
                        "<input ng-model=newGallery.children["+index+"].father.name ng-model-options='{ updateOn: blur}'>" +
                    "</div>" +
                    "<div class='label-input'>" +
                        "<label>Father's phone: </label>" +
                        "<input ng-model=newGallery.children["+index+"].father.phone ng-model-options='{ updateOn: blur}'>" +
                    "</div>" +
                    "<div class='label-input'>" +
                        "<label>Father's email: </label>" +
                        "<input ng-model=newGallery.children["+index+"].father.email ng-model-options='{ updateOn: blur}'>" +
                    "</div>" +
                "</div>" +
                "</div>")(scope));
        });
    };
});