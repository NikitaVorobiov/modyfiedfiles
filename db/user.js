var mongoose = require ("mongoose");
var bcrypt   = require('bcrypt-nodejs');
var theme = require("./theme");
var userSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String,
    groups: Array,
    sysType: String
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

//User = mongoose.model('user', userSchema);
exports.User = User = mongoose.model('user', userSchema);


exports.createUser = function(userArgs, callback){
    console.log("entering createUser");
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOne({ 'email' :  userArgs.email }, function(err, user) {
        // if there are any errors, return the error
        if (err){
            return callback(err);
        }
        // check to see if theres already a user with that email
        if (user) {
            return callback(null, false, 'That email is already taken.');
        } else {
            // if there is no user with that email
            // create the user
            var newUser = new User ({
                firstName: userArgs.firstName.replace(/%20/g, " "),
                lastName: userArgs.lastName.replace(/%20/g, " "),
                email: userArgs.email,
                password: userSchema.methods.generateHash(userArgs.password),
                groups: userSchema.groups,
                sysType: userArgs.sysType
            });
            newUser.save(function (err) {
                if (err){
                    console.log ('Error on save! err = ' + err);
                    callback(err);
                } else {
                    console.log('saved!');
                    theme.createInitThemes(newUser._id);
                    callback(null, newUser);
                }
            });
        }
    });
};

exports.login = function(userArgs, callback){
    console.log("entering login");
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOne({ 'email' :  userArgs.email }, function(err, user) {
        // if there are any errors, return the error before anything else
        if (err){
            return callback(err);
        }
        // if no user is found, return the message
        if (!user){
            return callback(null, false, 'No user found.'); // req.flash is the way to set flashdata using connect-flash
        }
        // if the user is found but the password is wrong
        if (!user.validPassword(userArgs.password)){
            return callback(null, false, 'Oops! Wrong password.'); // create the loginMessage and save it to session as flashdata
        }
        // all is well, return successful user
        return callback(null, user);
    });
};

exports.getUser = function(id, callback){
    console.log("entering getUser");
    console.log("queryArgs are " + id);
    var query = User.findOne({_id: id});
    console.log("query is = ", query);
    query.exec(function(err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback(err);
        }
    });
};

exports.getAllUsers = function(callback){
    console.log("entering getAllUsers");
    var query = User.find({});
    console.log("query is = ", query);
    query.exec(function(err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback(err);
        }
    });
};

exports.removeAllUsers = function(callback){
    console.log("entering removeAllUsers");
    var query = User.remove({});
    console.log("query is = ", query);
    query.exec(function(err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback(err);
        }
    });
};

// exports.updateUserGroups = function(queryArgs, callback) {
//     var user = new User.update({_id: queryArgs.userId}, { $push: {
//         groupId: queryArgs.groupId,
//         groupName: queryArgs.groupName
//     }});
//     query.exec(function (err, result) {
//         if (!err) {
// 	        console.log("this ::::" + result);
// 	        callback(result);
//         } else {
// 	        console.log("err ::::" , errr);
// 	        callback(err);
//         }
//     });
// };

exports.updateUserGroups = function (data, callback) {
	console.log(data);
	console.log('entering in updateUserGroups');
	User.findByIdAndUpdate({_id: data.userId}, { $push: {groups: data.groupInfo}}, {'new': true}, function (err, result) {
		if (err) {
			console.log('error is:' + err);
			callback(err);
		} else {
			console.log('update: ' + result);
			callback(result);
		};
	});
};

exports.removeUser = function(id, callback){
    console.log("entering removeUser");
    console.log("id = ", id);
    User.remove({_id:mongoose.Types.ObjectId(id)});
    callback({_id:id});
};

exports.updateUser = function(userArgs, callback){
    console.log("entering updateUser");
    var user = new User ({
        _id: mongoose.Types.ObjectId(userArgs._id),
        firstName: userArgs.firstName.replace(/%20/g, " "),
        lastName: userArgs.lastName.replace(/%20/g, " "),
        email: userArgs.email
    });
    console.log("user = " + user);
    user.save(function (err) {
        if (err){
            console.log ('Error on save!');
            callback(err)
        } else {
            console.log ('saved!');
            callback(user);
        }
    });
};
