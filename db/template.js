var mongoose = require ("mongoose");
var templateSchema = new mongoose.Schema({
    userId: String,
    name: String,
    themeName: String,
    themes: [{_id:String, design:{coords:[{x:Number, y:Number}], bg:String}}]
});

Template = mongoose.model('template', templateSchema);

exports.createTemplate = function(templateArgs, callback){
    console.log("entering createTemplate");
    var newTemplate = new Template ({
        templateId: templateArgs.picbuzzId,
        templateName: templateArgs.name,
        photosLimit: templateArgs.phLimit,
        designerId: templateArgs.designerId
    });
    console.log("newTemplate = " + newTemplate);
    newTemplate.save(function (err) {
        if (err){
            console.log ('Error on save!');
            callback(err)
        } else {
            console.log ('saved!');
            callback(newTemplate);
        }
    });
};

exports.getTemplates = function(data, callback){
    console.log("entering getTemplates");
    var query = Template.find({userId:data.userId}, {name:true});
    console.log("query is = ", query);
    query.exec(function (err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"getTemplates failed"});
        }
    });
};

exports.getTemplateCont = function getTemplateCont (templateId, callback) {
    console.log('entering getTemplateCont');
    var query = Template.find({_id: templateId}, {name: true});
    console.log("query is = " + query);
    query.exac(function (err, result) {
        if (!err) {
            console.log('result is : ', result);
            callback(result);
        } else {
            console.log("error is : ", err);
            callback(err);
        }
    });
};

exports.getTemplateContent = function(templateArgs, callback){
    console.log("entering getTemplateContent");
    console.log("queryArgs are " + templateArgs);
    var query = Template.findOne({_id:mongoose.Types.ObjectId(templateArgs.templateId)});
    console.log("query is = ", query);
    query.exec(function(err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"Get template content failed"});
        }
    });
};

exports.updateTemplate = function(templateArgs, callback){
    console.log("entering updateTemplate");
    Template.findByIdAndUpdate(mongoose.Types.ObjectId(templateArgs._id),{$set:{name:templateArgs.name, themes: templateArgs.themes}}, function (err, result) {
        if (err){
            console.log("error is: " + err);
            callback({error:"Update template failed"});
        } else {
            console.log ('update!');
            callback(result);
        }
    });
};

exports.deleteTemplate = function(templateArgs, callback){
    console.log("entering deleteTemplate");
    console.log("templateArgs = ", templateArgs);
    Template.remove({_id:mongoose.Types.ObjectId(templateArgs._id)}, function(err, result){
        if (err || !result){
            if(err){
                console.log("error is: " + err);
            } else {
                console.log("result is: " + result);
            }
            callback({error:"delete template failed"});
        } else {
            console.log ('delete!');
            callback({deleted: result});
        }
    });
};

exports.getAllTemplates = function(callback){
    console.log("entering getAllTemplates");
    var query = Template.find({});
    console.log("query is = ", query);
    query.exec(function (err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"getAllTemplates failed"});
        }
    });
};

exports.removeAllTemplates = function(callback){
    console.log("entering removeAllTemplates");
    var query = Template.remove({});
    console.log("query is = ", query);
    query.exec(function(err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"removeAllTemplates failed"});
        }
    });
};
