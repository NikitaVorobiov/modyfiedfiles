var mongoose = require ("mongoose");
var themeSchema = new mongoose.Schema({
    userId: {type:String},
    name: {type:String},
    designs: [{coords:[{x:Number, y:Number}]}]
});

themeSchema.index({userId: 1, name: 1}, {unique: true});

Theme = mongoose.model('theme', themeSchema);

exports.createInitThemes = function(userId){
    console.log("entering createInitThemes");
    console.log("userId = " + userId);
    Theme.create([
        {userId: userId, name: "Rosh HaShana", designs: []},
        {userId: userId, name: "Sukot", designs: []},
        {userId: userId, name: "Chanuka", designs: []},
        {userId: userId, name: "Purim", designs: []},
        {userId: userId, name: "Pesach", designs: []},
        {userId: userId, name: "Sports", designs: []},
        {userId: userId, name: "Tiul Shnati", designs: []}
    ],function(err){
        if (err){
            console.log("error is: " + err);
        } else {
            console.log ('Created init themes!');
        }
    });
};

exports.getOrCreateThemes = function(themeArgs, callback){
    console.log("entering getOrCreateThemes");
    console.log("themeArgs = ", themeArgs);
    var indexMap = {};
    themeArgs.themesNames.forEach(function(theme, index){
        indexMap[theme.name] = index;
    });
    var query = Theme.find({
        'name': { $in: themeArgs.themesNames}}).where('userId').equals(themeArgs.userId);
    query.exec(function(err, foundThemes) {
        if (!err) {
            console.log("result is: " + foundThemes);
            var foundThemesMap = {};
            foundThemes.forEach(function(theme){
                foundThemesMap[theme.name] = theme;
            });
            var newThemes = [];
            themeArgs.themesNames.forEach(function(themeName){
                if(!foundThemesMap[themeName]){
                    newThemes.push({userId: themeArgs.userId, name: themeName, designs: []});
                }
            });
            if(newThemes.length){
                exports.createThemes(newThemes, function(dbNewThemes, err){
                    if(!err){
                        var dbNewThemesMap = {};
                        dbNewThemes.forEach(function(theme){
                            dbNewThemesMap[theme.name] = theme;
                        });
                        var returnedThemes = [];
                        themeArgs.themesNames.forEach(function(themeName){
                            if(dbNewThemesMap[themeName]){
                                returnedThemes.push(dbNewThemesMap[themeName]);
                            } else if(foundThemesMap[themeName]){
                                returnedThemes.push(foundThemesMap[themeName]);
                            }
                        });
                        callback(returnedThemes);
                    } else {
                        console.log("error is: " + err.errmsg);
                        callback(err.errmsg);
                    }
                });
            } else {
                callback(foundThemes);
            }
        } else {
            console.log("error is: " + err);
            callback(err);
        }
    });
};

exports.createThemes = function(themeArgs, callback){
    console.log("entering createThemes");
    console.log("themeArgs = ", themeArgs);
    Theme.create(themeArgs,function(err, newThemes){
        if (err){
            console.log ('Error on save!');
            callback(null, err)
        } else {
            console.log ('saved!');
            callback(newThemes);
        }
    });
};

exports.createTheme = function(themeArgs, callback){
    console.log("entering createTheme");
    var newTheme = new Theme ({
        userId: themeArgs.userId,
        name: themeArgs.name,
        designs: themeArgs.designs || []
    });
    console.log("newTheme = " + newTheme);
    newTheme.save(function (err) {
        if (err){
            console.log ('Error on save!');
            callback(err)
        } else {
            console.log ('saved!');
            callback(newTheme);
        }
    });
};

exports.getAllUserThemes = function(data, callback){
    console.log("entering getAllUserThemes");
    var query = Theme.find({userId:data.userId});
    console.log("query is = ", query);
    query.exec(function (err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"getAllUserThemes failed"});
        }
    });
};

exports.getAllThemesNames = function(data, callback){
    console.log("entering getAllThemesNames");
    var query = Theme.find({userId:data.userId},{name:1});
    console.log("query is = ", query);
    query.exec(function (err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"getAllThemesNames failed"});
        }
    });
};

exports.getTheme = function(id, callback){
    console.log("entering getTheme");
    console.log("queryArgs are " + id);
    var query = Theme.findOne({_id:mongoose.Types.ObjectId(id)});
    console.log("query is = ", query);
    query.exec(function(err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback(err);
        }
    });
};

exports.updateTheme = function(themeArgs, callback){
    console.log("entering updateTheme");
    var theme = new Theme ({
        _id: mongoose.Types.ObjectId(themeArgs._id),
        name: themeArgs.name,
        designs: themeArgs.designs
    });
    console.log("theme = " + theme);
    theme.save(function (err) {
        if (err){
            console.log ('Error on save!');
            callback(err)
        } else {
            console.log ('saved!');
            callback(theme);
        }
    });
};

exports.updateThemeDesign = function(themeArgs, callback){
    console.log("entering updateThemeDesign");
    Theme.update({_id: themeArgs.themeId, "designs._id": themeArgs.designId}, {"$set" : {"designs.$.coords": themeArgs.coords}},
    function(err, numAffected){
        if (!err){
            console.log ('update!');
            callback({numAffected:numAffected});
        } else {
            console.log ('Error on update!');
            callback({error:"Update theme design failed"});
        }

    });
};

exports.addThemeDesign = function(themeArgs, callback){
    console.log("entering addThemeDesign");
    Theme.findByIdAndUpdate(
        themeArgs.themeId,
        {$push: {designs: {coords:[]}}},
        {safe: true, upsert: true, select:{designs:true}},
        function(err, data) {
            if (!err){
                console.log ('saved!');
                var designs = data._doc.designs;
                callback({design:designs[designs.length-1] || []});
            } else {
                console.log ('Error on save!');
                callback({error:"Add theme design failed"});
            }
        }
    );
};

exports.removeTheme = function(id, callback){
    console.log("entering removeTheme");
    console.log("id = ", id);
    Theme.remove({_id:mongoose.Types.ObjectId(id)});
    callback({_id:id});
};

exports.getAllThemes = function(callback){
    console.log("entering getAllThemes");
    var query = Theme.find({});
    console.log("query is = ", query);
    query.exec(function (err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"getAllThemes failed"});
        }
    });
};

exports.removeAllThemes = function(callback){
    console.log("entering removeAllThemes");
    var query = Theme.remove({});
    console.log("query is = ", query);
    query.exec(function(err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"removeAllThemes failed"});
        }
    });
};