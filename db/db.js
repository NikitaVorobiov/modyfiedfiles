var mongoose = require ("mongoose");
var conString =
	process.env.MONGOLAB_URI ||
	process.env.MONGOHQ_URL ||
	'mongodb://localhost/27017';

exports.connect = function() {
	mongoose.connect(conString, function (err) {
		if (err) {
			console.log ('ERROR connecting to: ' + conString + '. ' + err);
		} else {
			console.log ('Succeeded connected to: ' + conString);
		}
	});
};

exports.user = require("./user");
exports.gallery = require("./gallery");
exports.theme = require("./theme");
exports.template = require("./template");
exports.album = require("./album");
