var mongoose = require ("mongoose");
var albumSchema = new mongoose.Schema({
    userId: String,
    galleryId: String,
    childId: String,
    templateId: String,
    name: String,
    themes:[{_id:String, displayedPhotos:[{src:String, filters:{}}], design:{coords:[{x:Number, y:Number}], bg:String}}]
});

Album = mongoose.model('album', albumSchema);

exports.saveAlbum = function(albumArgs, callback){
    console.log("entering saveAlbum");
    if(!albumArgs._id){
        console.log("creating new album");
        var newAlbum = new Album ({
            userId: albumArgs.userId,
            galleryId: albumArgs.galleryId,
            childId: albumArgs.childId,
            templateId: albumArgs.templateId,
            name: albumArgs.name,
            themes:albumArgs.themes
        });
        console.log("newAlbum = " + newAlbum);
        newAlbum.save(function (err) {
            if (err){
                console.log ('Error on save!');
                callback(err)
            } else {
                console.log ('saved!');
                callback(newAlbum);
            }
        });
    } else {
        Album.update({ _id: mongoose.Types.ObjectId(albumArgs._id)}, albumArgs, function(err, result){
            if (err){
                console.log ('Error on update!');
                callback(err)
            } else {
                console.log ('updated!');
                callback(result);
            }
        });
    }
};

exports.createAlbums = function(albumsArgs, callback){
    console.log("entering createAlbums");
    var isMultiple =  albumsArgs.children.length > 1;
    var newAlbums = albumsArgs.children.map(function(child){
        return {
            userId: albumsArgs.userId,
            galleryId: albumsArgs.galleryId,
            childId: child.id,
            templateId: albumsArgs.templateId,
            name: isMultiple ? albumsArgs.name + "_" + child.name : albumsArgs.name,
            themes:child.themes
        };
    });
    Album.create(newAlbums, function(err, albums){
        if (err){
            console.log ('Error on insert!');
            callback(err)
        } else {
            console.log ('inserted!');
            callback(albums);
        }
    });
};

exports.getAlbums = function(data, callback){
    console.log("entering getAlbums");
    var query = Album.find({userId:data.userId});
    console.log("query is = ", query);
    query.exec(function (err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"getAlbums failed"});
        }
    });
};

exports.getAlbum = function(id, callback){
    console.log("entering getAlbum");
    console.log("queryArgs are " + id);
    var query = Album.findOne({_id:mongoose.Types.ObjectId(id)});
    console.log("query is = ", query);
    query.exec(function(err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback(err);
        }
    });
};

exports.updateAlbum = function(albumArgs, callback){
    console.log("entering updateAlbum");
    Album.findByIdAndUpdate(mongoose.Types.ObjectId(albumArgs._id),{$set:{themes: albumArgs.themes}}, function (err, result) {
        if (err){
            console.log("error is: " + err);
            callback({error:"Update album failed"});
        } else {
            console.log ('update!');
            callback(result);
        }
    });
};

exports.deleteAlbum = function(albumArgs, callback){
    console.log("entering removeAlbum");
    console.log("albumArgs = ", albumArgs);
    Album.remove({_id:mongoose.Types.ObjectId(albumArgs._id)}, function(err, result){
        if (err || !result){
            if(err){
                console.log("error is: " + err);
            } else {
                console.log("result is: " + result);
            }
            callback({error:"delete album failed"});
        } else {
            console.log ('delete!');
            callback({deleted: result});
        }
    });
};

exports.getAlbumContent = function(albumArgs, callback){
    console.log("entering getAlbumContent");
    console.log("queryArgs are " + albumArgs);
    var query = Album.findOne({_id:mongoose.Types.ObjectId(albumArgs.albumId)});
    console.log("query is = ", query);
    query.exec(function(err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"Get album content failed"});
        }
    });
};


exports.getAllAlbums = function(callback){
    console.log("entering getAllAlbums");
    var query = Album.find({});
    console.log("query is = ", query);
    query.exec(function (err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"getAllAlbums failed"});
        }
    });
};

exports.removeAllAlbums = function(callback){
    console.log("entering removeAllAlbums");
    var query = Album.remove({});
    console.log("query is = ", query);
    query.exec(function(err, result) {
        if (!err) {
            console.log("result is: " + result);
            callback(result);
        } else {
            console.log("error is: " + err);
            callback({error:"removeAllAlbums failed"});
        }
    });
};