var mongoose = require ("mongoose");
var gallerySchema = new mongoose.Schema({
	userId: String,
	name: String,
	members: Array,
	children: [{
			name: String,
			mother: {name: String, email: String, phone: String},
			father: {name: String, email: String, phone: String},
			photos: []
	}],
	templates: Array
});

Gallery = mongoose.model('gallery', gallerySchema);

exports.createGallery = function(galleryArgs, callback){
	console.log("entering createGallery");
	var newGallery = new Gallery ({
		userId: galleryArgs.userId,
		name: galleryArgs.name,
		children: galleryArgs.children || [],
		members: [],
		templates: []
	});
	newGallery.members.push(galleryArgs.memberInfo);
	console.log("newGallery = " + newGallery);
	newGallery.save(function (err) {
		if (!err){
			console.log ('saved!');
			callback(newGallery);
		} else {
			console.log(newGallery);
			console.log($rootScope.currentUser)
			console.log ('Error on save!');
			callback({error:"Create gallery failed"});
		}
	});
};

exports.getGalleryContent = function(galleryArgs, callback){
	console.log("entering getGalleryContent");
	console.log("queryArgs are " + galleryArgs);
	var query = Gallery.findOne({_id:mongoose.Types.ObjectId(galleryArgs.galleryId)});
	console.log("query is = ", query);
	query.exec(function(err, result) {
		if (!err) {
			console.log("result is: " + result);
			callback(result);
		} else {
			console.log("error is: " + err);
			callback({error:"Get gallery content failed"});
		}
	});
};

exports.updateGallery = function(galleryArgs, callback){
	console.log("entering updateGallery");
	console.log("queryArgs are " + galleryArgs);
	Gallery.findByIdAndUpdate(mongoose.Types.ObjectId(galleryArgs.galleryId),{$set:{name:galleryArgs.name}, $push: {children: {$each:galleryArgs.children}}}, { 'new': true}, function (err, result) {
		if (err){
			console.log("error is: " + err);
			callback({error:"Update gallery failed"});
		} else {
			console.log ('update!');
			callback(result);
		}
	});
};

exports.addNewTpl = function (data, callback) {
	console.log("Entering in addNewTpl");
	console.log("queryArgs are : " + data);
	Gallery.findByIdAndUpdate(mongoose.Types.ObjectId(data.groupId), {$push: {templates: data.templateInfo}}, {'new': true}, function (err, result) {
		if (err) {
			console.log('error is: ' + err);
			callback(err);
		} else {
			console.log('update: ' + result);
			callback(result);
		}
	});

	console.log("HERE >>>>>>>> " + data);
}

exports.updateGroupMembers = function (data, callback) {
	console.log(data);
	console.log('entering in updateGroupMembers');
	Gallery.findByIdAndUpdate(mongoose.Types.ObjectId(data._id), { $push: {members: data.memberInfo}}, {'new': true}, function (err, result) {
		if (err) {
			console.log('error is:' + err);
			callback(err);
		} else {
			console.log('update: ' + result);
			callback(result);
		};
	});
};

exports.updateGroupChildren = function updateGroupChildren (data, callback) {
	data.childInfo._id = mongoose.Types.ObjectId();
	console.log('query is : ' + data);
	console.log('entering in updateGroupChildren');
	Gallery.findByIdAndUpdate(mongoose.Types.ObjectId(data.groupId), { $push: {children: data.childInfo}}, {'new': true}, function (err, result) {
		if (err) {
			console.log('error is: ' + err);
			callback(err);
		} else {
			console.log('update :' + result);
			callback(result);
		}
	});
};

exports.deleteGallery = function(galleryArgs, callback){
	console.log("entering deleteGallery");
	console.log("albumArgs = ", galleryArgs);
	Gallery.remove({_id:mongoose.Types.ObjectId(galleryArgs.galleryId)}, function(err, result){
		if (err || !result){
			if(err){
				console.log("error is: " + err);
			} else {
				console.log("result is: " + result);
			}
			callback({error:"delete gallery failed"});
		} else {
			console.log ('delete!');
			callback(result);
		}
	});
};

exports.getAllGalleries = function(callback) {
	console.log("entering getAllGalleries");
	var query = Gallery.find({});
	console.log("query is = ", query);
	query.exec(function (err, result) {
		if (!err) {
			console.log("result is: " + result);
			callback(result);
		} else {
			console.log("error is: " + err);
			callback({error:"getAllGalleries failed"});
		}
	});
};

exports.getGalleries = function(groupId, callback){
	console.log("entering getGalleries");
	var query = Gallery.find({_id: groupId});
	console.log("query is = ", query);
	query.exec(function (err, result) {
		if (!err) {
			console.log("result is: " + result);
			callback(result);
		} else {
			console.log("error is: " + err);
			callback({error:"getGalleries failed"});
		}
	});
};

exports.getChildInGallery = function(data, callback){
	console.log("entering getChildInGallery");
	var query = Gallery.findOne({userId:data.userId, _id:mongoose.Types.ObjectId(data.galleryId)}, {children:true});
	console.log("query is = ", query);
	query.exec(function (err, result) {
		if (!err) {
			console.log("result is: " + result);
			var targetChild;
			result.children.forEach(function(child){
				if(child.id == data.childId){
					targetChild = child;
					return false;
				}
			});
			if(targetChild){
				callback(targetChild);
			} else {
				callback({error:"getChildInGallery failed: Couldn't find child"});
			}
		} else {
			console.log("error is: " + err);
			callback({error:"getChildInGallery failed: Couldn't find gallery"});
		}
	});
};

exports.removeAllGalleries = function(callback){
	console.log("entering removeAllGalleries");
	var query = Gallery.remove({});
	console.log("query is = ", query);
	query.exec(function(err, result) {
		if (!err) {
			console.log("result is: " + result);
			callback(result);
		} else {
			console.log("error is: " + err);
			callback({error:"removeAllGalleries failed"});
		}
	});
};

exports.deleteChild = function(galleryArgs, callback){
	console.log("entering deleteChild");
	console.log("galleryArgs = ", galleryArgs);
	Gallery.update(
		{ _id: galleryArgs.galleryId },
		{ $pull: { 'children': { _id: galleryArgs.childId } } },
		function(err, result){
			if (err || !result){
				if(err){
					console.log("error is: " + err);
				} else {
					console.log("result is: " + result);
				}
				callback({error: err});
			} else {
				console.log ('deleted!');
				callback({deleted: result});
			}
		}
	);
};
